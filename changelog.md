1.17.0
------
- Afegit un LocalizedString per poder afegir strings localitzats

1.16.0
------
- Afegida l'opció d'eliminar registres associats:
  - Nova opció "remove" (ara existeix pull, field i remove)

1.15.3
------
- Afegit al Request l'opció d'obtenir els $_FILES enviats al servidor

1.15.2
------
- Afegit al Request l'opció de carregar del $_POST (quan el php://input no está disponible)

1.15.1
------
- Afegit un PHONE_DEFAULT_REGION al Phone

1.15.0
------
- Afegit un nou Logger per poder integrar amb eines com el Rollbar o similars
  - Actualment només es fa servir en el Response quan hi ha un error

1.14.1
------
- Afegit el mètode **getOwnerUser()** a ApiUser

1.14.0
------
- Canviada la informació que retorna el login
  - Eliminats els properties "publicId" i "ownerName"

1.13.2
------
- El SessionController ara és més personalitzable

1.13.1
------
- Afegit el header "api-response" al CORS allowed headers

1.13.0
------
- Afegits els mètodes **isRunningFromCli** i **isRunningInDocker** a la classe **Environment**
  - isRunningFromCli: Comprova si s'està executant l'script des del CLI
  - isRunningInDocker: Comprova si s'està executant un PHP dins un docker 

1.12.0
------
- Afegida la classe **FilterSortField** per gestionar els camps a ordenar
- Implementat un sistema per ordenar els resultats dels filtres fent servir **FilterSortField**

1.11.1
------
- Afegit l'enum **FilterSortDirection** per fer fàcil ordenar els resultats dels filtres

1.11.0
------
- Afegits els mètodes **count** i **countActive** als controladors CRUD
- Afegit el mètode **filter** per obtenir resultats aplicant filtres

1.10.0
------
- Afegit un alias **::as** al request, per fer més curta la crida del **getDataAs**

1.9.0
-----
- Afegit un nou mètode **getDataAs** per obtenir el data com un objecte concret

1.8.0
-----
- Afegida la possibilitat de marcar com a "NoContent" un Response
  - Afegit també el mètode **successNoContentResponse**
- Implementat el mètode **canManage** als controladors CRUD
  - Aquest mètode serveix per poder "preguntar" si una acció del controlador está disponible per l'usuari actual o no

1.7.0
-----
- Implementada nova forma de gestionar els CRUDControllers
	- Ara es pot distingir si esta fent Create, Edit o Delete i ajustar els permisos
	- Ara es pot fer override del **restrictAccessByLevel** (abans era private)

1.6.0
-----
- Afegit un nou "option -> excluding" als Request
  - Aquest property defineix si s'estan excloent o no els "fields"

1.5.0
-----
- Implementada la funcionalitat de filtrar el data dels Response
  - Ara es pot passar dins del Request->options una opció "fields" que serveix per filtrar els properties que es retornen dins del Data

1.4.0
-----
- Afegida nova classe **ExplicitTypedPlainEntity**

1.3.0
-----
- Millores per fer més fàcil heredar i fer override de la llibreria

1.2.0
-----
- Millores generals

1.1.0
-----
- Actualitzat el codi per suportar el nou Foundation core 2.9


1.0.0
-----
- Versió inicial.
