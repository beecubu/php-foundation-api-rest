<?php

namespace Beecubu\Foundation\ApiRest\Core\Persistence;

use Beecubu\Foundation\ApiRest\Core\Entities\Environment\Environment;

defined('MONGO_DATABASE') or define('MONGO_DATABASE', 'api-rest');

/**
 * La connexió amb la Base de Dades.
 */
class DBConnection extends \Beecubu\Foundation\MongoDB\DBConnection\DBConnection
{
    public function isSandboxMode(): bool
    {
        return Environment::current()->isSandboxMode();
    }
}
