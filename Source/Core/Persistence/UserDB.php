<?php

namespace Beecubu\Foundation\ApiRest\Core\Persistence;

use Beecubu\Foundation\ApiRest\Core\Entities\Users\ApiUser;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\OwnerUser;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\User;
use Beecubu\Foundation\MongoDB\Driver\MongoCollection;
use Beecubu\Foundation\MongoDB\Entity;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\Regex;
use function Beecubu\Foundation\Helpers\Id\validateObjectId;

/**
 * Els User a la BD.
 */
class UserDB extends DBConnection
{
    /** @var static[] */
    protected static $instance = [];

    protected static function instanceKey(): string
    {
        return self::class;
    }

    /**
     * Devuelve el repo actual.
     *
     * @return static El repo actual.
     */
    public static function current(): self
    {
        $key = static::instanceKey();
        // first time?
        if ( ! isset(self::$instance[$key]))
        {
            self::$instance[$key] = new static();
        }
        return self::$instance[$key];
    }

    // Private methods

    protected function db(): MongoCollection
    {
        return $this->db->users;
    }

    protected function deepSearchQueries(Entity $entity): array
    {
        return [];
    }

    // Public methods

    /**
     * Guarda els canvis d'un usuari.
     *
     * @param User $user L'usuari a guardar els canvis.
     */
    public function save(User $user): void
    {
        if ($data = $user->rawData())
        {
            if ($this->db()->save($data))
            {
                $user->set_ivar('id', $data->_id);
            }
        }
    }

    /**
     * Elimina un usuari de la BD.
     *
     * @param User $user L'usuari a eliminar.
     */
    public function delete(User $user): void
    {
        if ($user->hasId())
        {
            // delete all the usages of this object
            foreach ($this->deepSearchQueries($user) as $queryInfo)
            {
                // pull out from array
                if ($queryInfo['action'] === 'pull')
                {
                    $queryInfo['db']->update($queryInfo['query'], ['$pull' => $queryInfo['delete']], ['multi' => true]);
                }
                elseif ($queryInfo['action'] === 'field') // unset value the field
                {
                    $queryInfo['db']->update($queryInfo['query'], ['$set' => [$queryInfo['delete'] => null]], ['multi' => true]);
                }
                elseif ($queryInfo['action'] === 'remove') // remove the document
                {
                    $queryInfo['db']->remove($queryInfo['query']);
                }
            }
            // finally delete the main object
            $this->db()->remove(['_id' => new ObjectId($user->id)]);
        }
    }

    /**
     * Devuelve todos los usuarios registrados.
     *
     * @return User[] Els usuaris a la BD.
     */
    public function allUsers(): array
    {
        $array = [];
        // get all users
        if ($data = $this->db()->find())
        {
            foreach ($data as $rawData)
            {
                $array[] = User::instanceWithRawData($rawData);
            }
        }
        // all the users
        return $array;
    }

    /**
     * Tots els usuaris d'un propietari.
     *
     * @param OwnerUser $ownerUser
     *
     * @return ApiUser[]
     */
    public function allUsersByOwner(OwnerUser $ownerUser): array
    {
        $array = [];
        // get all users
        if ($data = $this->db()->find(['ownerId' => $ownerUser->id]))
        {
            foreach ($data as $rawData)
            {
                $array[] = User::instanceWithRawData($rawData);
            }
        }
        // all the users
        return $array;
    }

    /**
     * Devuelve los usuarios filtrados de la DB.
     *
     * @param int $limit El número de resultados.
     * @param int $skip El numero de resultados a ignorar.
     * @param string|null $class El tipo de usuario que se busca.
     *
     * @return array
     */
    public function filteredUsers(int $limit, int $skip, ?string $class): array
    {
        // sort options
        $options = ['sort' => ['creationDate' => -1]];
        // pagination options
        if ($limit > 0)
        {
            $options += ['limit' => $limit];
        }
        if ($skip > 0)
        {
            $options += ['skip' => $skip];
        }
        // init query and results
        $query = [];
        $results = [];
        // prepare the query
        if ($class) $query['_class'] = $class;
        // get all rooms
        if ($data = $this->db()->find($query, [], $options))
        {
            foreach ($data as $rawData)
            {
                $results[] = User::instanceWithRawData($rawData);
            }
        }
        return $results;
    }

    /**
     * Obté un usuari a partir del seu id.
     *
     * @param string $id L'id de l'usuari.
     *
     * @return User L'usuari demanat.
     */
    public function byId(string $id): ?User
    {
        if (validateObjectId($id) && $data = $this->db()->findOne(['_id' => new ObjectId($id)]))
        {
            return User::instanceWithRawData($data);
        }
        return null;
    }

    /**
     * Obtiene un usuario a partir de su correo.
     *
     * @param string $email El correo.
     *
     * @return ApiUser|null
     */
    public function byEmail(string $email): ?ApiUser
    {
        if ($data = $this->db()->findOne(['email' => new Regex("^$email$", 'i')]))
        {
            return ApiUser::instanceWithRawData($data);
        }
        return null;
    }

    /**
     * Obtiene un usuario a partir de su flash id.
     *
     * @param string $id El flash id.
     *
     * @return ApiUser|null
     */
    public function byFlashId(string $id): ?ApiUser
    {
        if ($data = $this->db()->findAndModify(['flashId' => $id], ['$unset' => ['flashId' => 1]]))
        {
            unset($data->flashId);
            // create the login user
            return ApiUser::instanceWithRawData($data);
        }
        return null;
    }

    /**
     * Obté un usuari propietari a partir del seu publicId.
     *
     * @param string $publicId El públic id.
     *
     * @return OwnerUser|null
     */
    public function byPublicId(string $publicId): ?OwnerUser
    {
        if ($data = $this->db()->findOne(['publicId' => $publicId]))
        {
            return OwnerUser::instanceWithRawData($data);
        }
        return null;
    }

    /**
     * Comprova si un email es pot fer servir per assignar-lo a un usuari o no.
     *
     * @param string $email L'email a comprovar.
     * @param string|null $id L'id de l'usuari al qui es vol assignar el correu.
     *
     * @return bool
     */
    public function emailCanBeUsed(string $email, ?string $id): bool
    {
        if ($data = $this->db()->findOne(['email' => new Regex("^$email$", 'i')], ['_id' => 1]))
        {
            return $id && $id === (string)$data->_id;
        }
        return true;
    }
}
