<?php

namespace Beecubu\Foundation\ApiRest\Core\Persistence;

use Beecubu\Foundation\ApiRest\Core\Entities\IPInfo\IPInfo;
use Beecubu\Foundation\MongoDB\Driver\MongoCollection;

class IPInfoCacheDB extends DBConnection
{
    /**
     * Retorna la instancia actual del repositori.
     *
     * @return static La instància del repositori.
     */
    public static function current(): self
    {
        static $instance = null;
        // first time?
        if ( ! $instance)
        {
            $instance = new static('common');
        }
        return $instance;
    }

    /**
     * Obté la informació d'una IP que ja s'havia consultat abans.
     *
     * @param string $ip La IP a consultar.
     *
     * @return IPInfo La informació de la IP.
     */
    public static function ipInfoByIP(string $ip): ?IPInfo
    {
        return self::current()->getIpInfoByIP($ip);
    }

    /**
     * Guarda la informació d'una IP.
     *
     * @param IPInfo $info La informació a guardar.
     */
    public static function saveIPInfo(IPInfo $info): void
    {
        self::current()->doSaveIPInfo($info);
    }

    // Private methods

    private function db(): MongoCollection
    {
        return $this->db->ip_info_cache;
    }

    private function getIpInfoByIP(string $ip): ?IPInfo
    {
        if ($data = $this->db()->findOne(['_id' => $ip]))
        {
            return IPInfo::instanceWithRawData($data);
        }
        return null;
    }

    private function doSaveIPInfo(IPInfo $info): void
    {
        $this->db()->save($info->rawData());
    }
}
