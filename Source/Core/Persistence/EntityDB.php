<?php

namespace Beecubu\Foundation\ApiRest\Core\Persistence;

use Beecubu\Foundation\ApiRest\Api\Entities\Filter\Filter;
use Beecubu\Foundation\ApiRest\Api\Entities\Filter\FilterResult;
use Beecubu\Foundation\ApiRest\Api\Entities\Filter\FilterSortDirection;
use Beecubu\Foundation\ApiRest\Api\Entities\Filter\FilterSortField;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\OwnerUser;
use Beecubu\Foundation\MongoDB\Driver\MongoCollection;
use Beecubu\Foundation\MongoDB\Entity;
use MongoDB\BSON\ObjectId;
use stdClass;
use function Beecubu\Foundation\Helpers\Id\validateObjectId;

/**
 * El Entity DB básico para la DB.
 */
abstract class EntityDB extends DBConnection
{
    /** @var Entity|string $class */
    protected $class;

    abstract protected function db(): MongoCollection;

    abstract protected static function instanceKey(): string;

    protected static $instance = [];

    public static    $cacheEnabled = true;
    protected static $cache        = [];

    protected $plainObjectId = false;

    /**
     * Retorna la instancia actual del repositori.
     *
     * @return static La instància del repositori.
     */
    public static function current(): self
    {
        $key = static::instanceKey();
        // first time?
        if ( ! isset(self::$instance[$key]))
        {
            self::$instance[$key] = new static();
        }
        return self::$instance[$key];
    }

    /**
     * Guarda los cambios del objeto.
     *
     * @param Entity $entity El objeto a guardar.
     */
    public function save(Entity $entity): void
    {
        if ($data = $entity->rawData())
        {
            if ($this->db()->save($data))
            {
                $entity->set_ivar('id', $data->_id);
            }
        }
    }

    /**
     * Aquest mètode es crida en el moment de fer un "get" i serveix per tunejar o modificar la consulta del "get".
     *
     * @param array $query La query original.
     *
     * @return array
     */
    protected function getQuery(array $query): array
    {
        return $query;
    }

    /**
     * Aquest mètode es crida abans de "crear" l'objecte final en el moment de fer "get".
     *
     * @param stdClass $data L'informació en crú de l'objecte.
     *
     * @return stdClass
     */
    protected function beforeInstantiateEntityData(stdClass $data): stdClass
    {
        return $data;
    }

    /**
     * Obté un objecte concret.
     *
     * @param string $id L'id de l'objecte a obtenir.
     *
     * @return Entity|null
     */
    public function get(string $id): ?Entity
    {
        if ($this->plainObjectId || validateObjectId($id))
        {
            $key = static::class.'@'.$id;
            // use a cached version...
            if (self::$cacheEnabled && $obj = self::$cache[$key] ?? null)
            {
                return $obj;
            }
            // the id
            $_id = $this->plainObjectId ? $id : new ObjectId($id);
            // get it from DB
            if ($data = $this->db()->findOne($this->getQuery(['_id' => $_id])))
            {
                $obj = $this->class::instanceWithRawData($this->beforeInstantiateEntityData($data));
                self::$cache[$key] = $obj;
                return $obj;
            }
        }
        return null;
    }

    /**
     * Elimina el objeto.
     *
     * @param Entity $entity
     *
     * @return bool
     */
    public function delete(Entity $entity): bool
    {
        if ($entity->hasId())
        {
            // delete all the usages of this object
            foreach ($this->deepSearchQueries($entity) as $queryInfo)
            {
                // pull out from array
                if ($queryInfo['action'] === 'pull')
                {
                    $queryInfo['db']->update($queryInfo['query'], ['$pull' => $queryInfo['delete']], ['multi' => true]);
                }
                elseif ($queryInfo['action'] === 'field') // unset value the field
                {
                    $queryInfo['db']->update($queryInfo['query'], ['$set' => [$queryInfo['delete'] => null]], ['multi' => true]);
                }
                elseif ($queryInfo['action'] === 'remove') // remove the document
                {
                    $queryInfo['db']->remove($queryInfo['query']);
                }
            }
            // finally delete the main object
            if ($result = ($this->db()->remove(['_id' => new ObjectId($entity->id)])->getDeletedCount() === 1))
            {
                if ($entity instanceof \Beecubu\Foundation\ApiRest\Core\Entities\Entity\Entity)
                {
                    $entity->afterDelete();
                }
                // remove from cache
                $key = self::class.'@'.$entity->id;
                if (isset(self::$cache[$key])) unset(self::$cache[$key]);
            }
            // finish
            return $result;
        }
        return false;
    }

    /**
     * Obtiene todos los objetos.
     *
     * @param string $id El id propietario.
     *
     * @return Entity[] Los objetos.
     */
    public function all(string $id): array
    {
        $results = [];
        // prepare the sort options
        $sort = ['index' => 1] + $this->sortOptions();
        // get all
        if (validateObjectId($id) && $data = $this->db()->find(['ownerId' => $id], [], ['sort' => $sort]))
        {
            foreach ($data as $rawData)
            {
                $results[] = $this->class::instanceWithRawData($rawData);
            }
            return $results;
        }
        return [];
    }

    /**
     * Obtiene el número total de objetos.
     *
     * @param string $id El id propietario.
     *
     * @return int
     */
    public function count(string $id): int
    {
        // get all
        if (validateObjectId($id) && $count = $this->db()->count(['ownerId' => $id]))
        {
            return $count;
        }
        return 0;
    }

    /**
     * Obtiene todos los objetos ACTIVOS disponibles.
     *
     * @param string $id El id propietario.
     *
     * @return static[]
     */
    public function allActive(string $id): array
    {
        $results = [];
        // prepare the sort options
        $sort = ['index' => 1] + $this->sortOptions();
        // get all
        if (validateObjectId($id) && $data = $this->db()->find(['ownerId' => $id, 'active' => true], [], ['sort' => $sort]))
        {
            foreach ($data as $rawData)
            {
                $results[] = $this->class::instanceWithRawData($rawData);
            }
            return $results;
        }
        return [];
    }

    /**
     * Obté el nombre total d'objectes.
     *
     * @param string $id L'id del propietari.
     *
     * @return int
     */
    public function countActive(string $id): int
    {
        // get all active
        if (validateObjectId($id) && $count = $this->db()->count(['ownerId' => $id, 'active' => true]))
        {
            return $count;
        }
        return 0;
    }

    /**
     * Prepara la query per filtrar.
     *
     * @param Filter $filter El filtre.
     *
     * @return array
     */
    protected function getQueryByFilter(Filter $filter): array
    {
        $query = [];
        // the active condition
        if ($filter->active !== null)
        {
            $query['active'] = $filter->active;
        }
        // the query
        return $query;
    }

    /**
     * Obté el nom del field a ordenar.
     *
     * @param FilterSortField $sortField El field amb la info per ordenar.
     * @param array $sort L'array que conté tots els sort.
     *
     * @return void
     */
    protected function addFilterSortField(FilterSortField $sortField, array &$sort): void
    {
        $sort[$sortField->field] = FilterSortDirection::toInt($sortField->sortDir);
    }

    /**
     * Obté els objectes aplicant un filtre.
     *
     * @param string $id L'id del propietari.
     * @param Filter $filter El filtre.
     *
     * @return FilterResult
     */
    public function filtered(string $id, Filter $filter): FilterResult
    {
        $result = new FilterResult();
        // init the pagination
        $paginationSkip = $paginationLimit = [];
        // configure the pagination
        if ($filter->hasPageSize())
        {
            $paginationSkip = [['$skip' => ($filter->pageIndex - 1)*$filter->pageSize]];
            $paginationLimit = [['$limit' => $filter->pageSize]];
        }
        // init the default sort options (this will be replaced with user sort options)
        $sort = [['$sort' => ['index' => 1]]];
        if ($filter->hasSort())
        {
            $sortValues = [];
            // append sort values
            foreach ($filter->sort as $sortField)
            {
                $this->addFilterSortField($sortField, $sortValues);
            }
            // the sort for mongo
            $sort = [['$sort' => $sortValues]];
        }
        // is a valid id
        if (validateObjectId($id))
        {
            $query = [
                [
                    '$match' => array_merge(['ownerId' => $id], $this->getQueryByFilter($filter)),
                ],
                [
                    '$facet' => [
                        'totalCount' => [['$count' => 'total']],
                        'results'    => array_merge($sort, $paginationSkip, $paginationLimit),
                    ],
                ],
            ];
            // execute the query
            $data = $this->db()->aggregate($query)->toArray()[0];
            // get the total count
            $result->count = $data->totalCount[0]->total ?? 0;
            // parse results
            foreach ($data->results as $rawData)
            {
                $result->addResult($this->class::instanceWithRawData($rawData));
            }
        }
        // the results
        return $result;
    }

    /**
     * Devuelve un listado con todos los lugares donde mirar si se esta usando este objeto.
     *
     * @param Entity $entity El objeto a buscar.
     *
     * @return array
     */
    abstract protected function deepSearchQueries(Entity $entity): array;

    /**
     * Devuelve las opciones para ordenar los resultados.
     *
     * @return array
     */
    abstract protected function sortOptions(): array;

    /**
     * Comprueba si el objeto se está usando.
     *
     * @param Entity $entity El Objeto a comprobar si se está usando.
     *
     * @return bool
     */
    public function inUse(Entity $entity): bool
    {
        foreach ($this->deepSearchQueries($entity) as $queryInfo)
        {
            if ($queryInfo['db']->count($queryInfo['query']) > 0)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Asigna los indices.
     *
     * @param OwnerUser $user El usuario propietario.
     * @param string[] $ids El listado de ids.
     */
    public function assignIndexes(OwnerUser $user, array $ids): void
    {
        foreach ($ids as $index => $id)
        {
            $this->db()->update(['ownerId' => $user->id, '_id' => new ObjectId($id)], ['$set' => ['index' => $index]]);
        }
    }
}
