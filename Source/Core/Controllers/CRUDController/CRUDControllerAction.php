<?php

namespace Beecubu\Foundation\ApiRest\Core\Controllers\CRUDController;

use Beecubu\Foundation\Core\Enum;

class CRUDControllerAction extends Enum
{
    public const CREATE = 'create';
    public const EDIT   = 'edit';
    public const DELETE = 'delete';
}
