<?php

namespace Beecubu\Foundation\ApiRest\Core\Controllers\CRUDController;

use Beecubu\Foundation\ApiRest\Api\Entities\Filter\Filter;
use Beecubu\Foundation\ApiRest\Api\Entities\Filter\FilterResult;
use Beecubu\Foundation\ApiRest\Api\Exceptions\ResourceIsNotYoursException;
use Beecubu\Foundation\ApiRest\Api\Exceptions\ResourceNotFoundException;
use Beecubu\Foundation\ApiRest\Core\Controllers\SessionController;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\EntityWithOwner;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\InvalidIdentifierException;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\TryingToDeleteResourceInUseException;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\TryingToDeleteResourceInUseWithoutConfirmationException;
use Beecubu\Foundation\ApiRest\Core\Entities\Exceptions\RequiredPropertiesAreMissingException;
use Beecubu\Foundation\ApiRest\Core\Entities\ReusableSingleton\ReusableSingleton;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions\MissingSessionAuthException;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions\SessionForbiddenAccessException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\UnexpectedUserInstanceException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\UserDetailsType;
use stdClass;
use Throwable;

abstract class CRUDController extends ReusableSingleton
{
    /** @var EntityWithOwner|string $class */
    protected $class;

    /**
     * Retorna si l'usuari actual pot gestionar el controlador (Create/Edit/Delete).
     *
     * @param string|null $action L'acció de referència a comprovar.
     *
     * @return bool
     */
    public function canManage(?string $action): bool
    {
        try
        {
            // try to restrict access
            $this->restrictAccessByLevel($this->defaultAccessLevels($action ?? CRUDControllerAction::CREATE));
            // not exceptions thrown, all ok
            return true;
        }
        catch (Throwable $e)
        {
            return false;
        }
    }

    /**
     * S'assegura que l'usuari actual pugui accedir.
     *
     * @param string[] $level Els nivells d'usuari que poden accedir, [] = TOTS.
     *
     * @return void
     *
     * @throws SessionForbiddenAccessException
     */
    protected function restrictAccessByLevel(array $level): void
    {
        if (count($level) > 0)
        {
            if ( ! in_array(SessionController::current()->getUserAsApiUser()->getType(), $level))
            {
                throw new SessionForbiddenAccessException();
            }
        }
    }

    /**
     * Retorna els nivells acceptats per defecte.
     *
     * @param string $action L'acció que s'està executant.
     *
     * @return string[]
     */
    protected function defaultAccessLevels(string $action): array
    {
        return [UserDetailsType::Admin];
    }

    /**
     * Retorna la instància del controlador.
     *
     * @return static
     *
     * @throws MissingSessionAuthException
     * @throws SessionForbiddenAccessException
     */
    public static function current()
    {
        // ensure user is logged
        if ( ! SessionController::current()->isApiUserLogged())
        {
            throw new MissingSessionAuthException();
        }
        // ensure this user can access
        if (SessionController::current()->getUserAsApiUser()->isSuspended())
        {
            throw new SessionForbiddenAccessException();
        }
        // get the instance
        return parent::current();
    }

    /**
     * Crea un nou objecte.
     *
     * @param stdClass $data La informació de l'objecte.
     *
     * @return EntityWithOwner
     *
     * @throws RequiredPropertiesAreMissingException
     * @throws UnexpectedUserInstanceException
     * @throws SessionForbiddenAccessException
     */
    public function create(stdClass $data): EntityWithOwner
    {
        $this->restrictAccessByLevel($this->defaultAccessLevels(CRUDControllerAction::CREATE));
        // before create
        $this->beforeCreate($data);
        // execute the create command
        $entity = $this->class::create(SessionController::current()->getOwnerUser(), $data);
        // before save
        $this->beforeSaveEntity($entity, CRUDControllerAction::CREATE);
        // save changes
        $entity->save();
        // the created entity
        return $entity;
    }

    /**
     * Abans de crear un objecte.
     *
     * @param stdClass $data La informació en crú.
     *
     * @void
     */
    protected function beforeCreate(stdClass &$data): void
    {
        // nothing to do here…
    }

    /**
     * Abans de guardar els canvis de l'objecte.
     *
     * @param EntityWithOwner $entity El entity
     * @param string $action L'acció que s'està realitzant (CRUDControllerAction)
     *
     * @return void
     */
    protected function beforeSaveEntity(EntityWithOwner &$entity, string $action): void
    {
        // nothing to do here…
    }

    /**
     * Obté tots els objectes disponibles.
     *
     * @return EntityWithOwner[]
     *
     * @throws UnexpectedUserInstanceException
     */
    public function all(): array
    {
        return $this->class::all(SessionController::current()->getOwnerUser());
    }

    /**
     * Obté tots els objectes ACTIUS disponibles.
     *
     * @return EntityWithOwner[]
     *
     * @throws UnexpectedUserInstanceException
     */
    public function allActive(): array
    {
        return $this->class::allActive(SessionController::current()->getOwnerUser());
    }

    /**
     * Oté el recompte d'objectes disponibles.
     *
     * @return int
     *
     * @throws UnexpectedUserInstanceException
     */
    public function count(): int
    {
        return $this->class::count(SessionController::current()->getOwnerUser());
    }

    /**
     * Oté el recompte d'objectes ACTIUS disponibles.
     *
     * @return int
     *
     * @throws UnexpectedUserInstanceException
     */
    public function countActive(): int
    {
        return $this->class::countActive(SessionController::current()->getOwnerUser());
    }

    /**
     * Obté els objectes aplicant un filtre.
     *
     * @param stdClass $data El filtre a aplicar.
     *
     * @return FilterResult
     *
     * @throws UnexpectedUserInstanceException
     */
    public function filtered(stdClass $data): FilterResult
    {
        $filter = $this->getFilterInstance($data);
        // apply the filters
        return $this->class::filtered(SessionController::current()->getOwnerUser(), $filter);
    }

    /**
     * Abans de filtrar els objectes.
     *
     * @param stdClass $data El filtre a aplicar.
     *
     * @return Filter
     */
    protected function getFilterInstance(stdClass $data): Filter
    {
        return Filter::instanceWithJson($data, false, true);
    }

    /**
     * Modifica la informació de l'objecte.
     *
     * @param string $id Id de l'objecte a modificar.
     * @param stdClass $data Informació a modificar.
     *
     * @return EntityWithOwner
     *
     * @throws InvalidIdentifierException
     * @throws RequiredPropertiesAreMissingException
     * @throws ResourceIsNotYoursException
     * @throws ResourceNotFoundException
     * @throws UnexpectedUserInstanceException
     * @throws SessionForbiddenAccessException
     */
    public function edit(string $id, stdClass $data): EntityWithOwner
    {
        $this->restrictAccessByLevel($this->defaultAccessLevels(CRUDControllerAction::EDIT));
        // before create
        $this->beforeEdit($id, $data);
        // get the entity
        $entity = $this->get($id);
        // fill object properties
        $entity->fromJson($data, false, true);
        // before save
        $this->beforeSaveEntity($entity, CRUDControllerAction::EDIT);
        // save changes
        $entity->save();
        // the modified entity
        return $entity;
    }

    /**
     * Abans de modificar un objecte.
     *
     * @param string $id L'id de l'entity.
     * @param stdClass $data La informació en crú.
     *
     * @void
     */
    protected function beforeEdit(string &$id, stdClass &$data): void
    {
        // nothing to do here…
    }

    /**
     * Obté un objecte existent.
     *
     * @param string $id L'id de l'objecte.
     *
     * @return EntityWithOwner
     *
     * @throws ResourceIsNotYoursException
     * @throws ResourceNotFoundException
     * @throws InvalidIdentifierException
     * @throws UnexpectedUserInstanceException
     */
    public function get(string $id): EntityWithOwner
    {
        // execute the get command
        if ($obj = $this->class::byId($id))
        {
            if ($obj->ensureOwnerUserCanManageThis(SessionController::current()->getOwnerUser()))
            {
                return $obj;
            }
            throw new ResourceIsNotYoursException();
        }
        throw new ResourceNotFoundException();
    }

    /**
     * Elimina un objecte.
     *
     * @param string $id L'id de l'objecte a eliminar.
     * @param bool $confirm TRUE = En el cas que estigui en ús l'eliminarà, FALSE = generarà un error si s'està fent servir.
     *
     * @return bool
     * @throws InvalidIdentifierException
     * @throws ResourceIsNotYoursException
     * @throws ResourceNotFoundException
     * @throws TryingToDeleteResourceInUseException
     * @throws TryingToDeleteResourceInUseWithoutConfirmationException
     * @throws UnexpectedUserInstanceException
     * @throws SessionForbiddenAccessException
     */
    public function delete(string $id, bool $confirm): bool
    {
        $this->restrictAccessByLevel($this->defaultAccessLevels(CRUDControllerAction::DELETE));
        // before delete
        $this->beforeDelete($id, $confirm);
        // get the entity to delete
        $entity = $this->get($id);
        // before delete
        $this->beforeDeleteEntity($entity);
        // delete the entity
        return $this->get($id)->delete($confirm);
    }

    /**
     * Abans de modificar un objecte.
     *
     * @param string $id L'id de l'entity.
     * @param bool $confirm TRUE = Confirma que s'elimina, FALSE = no es confirma.
     *
     * @void
     */
    protected function beforeDelete(string &$id, bool &$confirm): void
    {
        // nothing to do here…
    }

    /**
     * Abans d'eliminar l'objecte.
     *
     * @param EntityWithOwner $entity L'entity que es vol esborrar.
     *
     * @return void
     */
    protected function beforeDeleteEntity(EntityWithOwner &$entity): void
    {
        // nothing to do here…
    }
}
