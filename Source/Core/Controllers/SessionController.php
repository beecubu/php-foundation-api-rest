<?php

namespace Beecubu\Foundation\ApiRest\Core\Controllers;

use Beecubu\Foundation\ApiRest\Core\Entities\ReusableSingleton\ReusableSingleton;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions\InvalidSessionAuthException;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions\MissingSessionAuthException;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions\SessionForbiddenAccessException;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Session;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\ApiUser;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\EmailAlreadyInUseException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\EmailIsMissingException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\PasswordIsMissingException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\UnexpectedUserInstanceException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\OwnerUser;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\User;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\UserDetailsType;
use stdClass;

/**
 * Gestiona les sessions dels usuaris.
 */
class SessionController extends ReusableSingleton
{
    /** @var Session|null */
    protected $session = null;

    // Class constructor

    protected static function instanceKey(): string
    {
        return self::class;
    }

    /**
     * Fa login a la plataforma.
     *
     * @param string $email L'email.
     * @param string $password La contrasenya.
     *
     * @throws EmailAlreadyInUseException
     * @throws EmailIsMissingException
     * @throws InvalidSessionAuthException
     * @throws PasswordIsMissingException
     * @throws SessionForbiddenAccessException
     */
    public function login(string $email, string $password): void
    {
        // try to get the requested user
        if ($user = $this->getUserByEmailAndPassword($email, $password))
        {
            $this->startSessionWithUser($user);
        }
        else // error
        {
            $this->logout();
            // throw error
            throw new InvalidSessionAuthException();
        }
    }

    /**
     * Executa internament el login i retorna l'usuari.
     *
     * @param string $email L'email.
     * @param string $password La contrasenya.
     *
     * @return ApiUser|null
     */
    protected function getUserByEmailAndPassword(string $email, string $password): ?ApiUser
    {
        if ($user = ApiUser::byEmailAndPassword($email, $password) and $user instanceof ApiUser)
        {
            return $user;
        }
        return null;
    }

    // Public methods

    /**
     * Configura la sessió amb la informació de l'usuari.
     *
     * @param ApiUser $user L'usuari.
     *
     * @throws SessionForbiddenAccessException
     * @throws EmailAlreadyInUseException
     * @throws EmailIsMissingException
     * @throws PasswordIsMissingException
     */
    public function startSessionWithUser(ApiUser $user): void
    {
        // finish session
        $this->logout();
        // is this user suspended?
        if ($user->isSuspended())
        {
            throw new SessionForbiddenAccessException();
        }
        // update the last login date
        $user->updateLastLoginDate();
        // save the current session
        $this->getSession()->userId = $user->id;
        // save the current session
        $this->getSession()->save();
    }

    /**
     * Realitza un logout.
     */
    public function logout(): void
    {
        if ($this->getSession()->isLogged())
        {
            $this->getSession()->delete();
        }
    }

    /**
     * @return Session|null
     */
    protected function getSession(): ?Session
    {
        if ( ! $this->session)
        {
            $this->session = Session::current();
        }
        // return the session
        return $this->session;
    }

    /**
     * Comprova si la sessió actual està activa o no.
     *
     * @return bool TRUE = Si, FALSE = no.
     */
    public function isLogged(): bool
    {
        return $this->getSession()->isLogged();
    }

    /**
     * Inicia una nova sessió a partir d'un flashId.
     *
     * @param string $id El flashId a buscar.
     *
     * @return ApiUser
     *
     * @throws EmailAlreadyInUseException
     * @throws EmailIsMissingException
     * @throws InvalidSessionAuthException
     * @throws PasswordIsMissingException
     * @throws SessionForbiddenAccessException
     */
    public function startSessionWithUserFlashId(string $id): ApiUser
    {
        if ($user = ApiUser::byFlashId($id))
        {
            $this->startSessionWithUser($user);
            // return the user
            return $user;
        }
        else // impossible start a new session
        {
            throw new InvalidSessionAuthException();
        }
    }

    /**
     * Simplement, mira si hi ha una sessió activa.
     *
     * @throws MissingSessionAuthException
     */
    public function hello(): void
    {
        // ensure user is logged
        if ( ! $this->isLogged())
        {
            throw new MissingSessionAuthException();
        }
    }

    /**
     * Obté la info bàsica de l'usuari.
     *
     * @return stdClass
     *
     * @throws MissingSessionAuthException
     */
    public function meBasic(): stdClass
    {
        return $this->me()->basicInformation();
    }

    /**
     * Obté la informació de l'usuari de la sessió.
     *
     * @return ApiUser
     *
     * @throws MissingSessionAuthException
     */
    public function me(): ApiUser
    {
        if ($user = $this->getUserAsApiUser())
        {
            return $user;
        }
        throw new MissingSessionAuthException();
    }

    /**
     * Retorna l'usuari que hi ha connectat com a ApiUser.
     *
     * @return ApiUser
     */
    public function getUserAsApiUser(): ?ApiUser
    {
        if ($user = $this->getUser() and $user instanceof ApiUser)
        {
            return $user;
        }
        return null;
    }

    /**
     * Retorna l'usuari que hi ha connectat actualment.
     *
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->getSession()->user;
    }

    /**
     * Guarda els canvis del perfil.
     *
     * @param stdClass $data La informació a guardar.
     *
     * @return ApiUser
     *
     * @throws EmailAlreadyInUseException
     * @throws EmailIsMissingException
     * @throws MissingSessionAuthException
     * @throws PasswordIsMissingException
     */
    public function saveProfile(stdClass $data): ApiUser
    {
        $me = $this->me();
        $me->fromJson($data, false, true);
        $me->save();
        return $me;
    }

    /**
     * Comprova si l'usuari de la sessió és un Admin o no.
     *
     * @return bool
     */
    public function isAdminUserLogged(): bool
    {
        return $this->isApiUserLogged() && $this->getUserAsApiUser()->getType() === UserDetailsType::Admin;
    }

    /**
     * Comprova si hi ha un usuari del tipus API a la sessió.
     *
     * @return bool
     */
    public function isApiUserLogged(): bool
    {
        return $this->isLogged() && $this->getUser() instanceof ApiUser;
    }

    /**
     * Obté l'usuari propietari de la sessió.
     *
     * @return OwnerUser|null
     *
     * @throws UnexpectedUserInstanceException
     */
    public function getOwnerUser(): ?OwnerUser
    {
        if ($user = $this->getUserAsApiUser())
        {
            return OwnerUser::userById($user->ownerId);
        }
        return null;
    }
}
