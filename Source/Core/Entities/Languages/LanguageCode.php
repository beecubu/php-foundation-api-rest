<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Languages;

use Beecubu\Foundation\Core\Enum;
use Locale;

/**
 * Defineix els diferents tipus d'idiomes.
 */
class LanguageCode extends Enum
{
    public const Catalan    = 'ca_ES';
    public const English    = 'en_US';
    public const Spanish    = 'es_ES';
    public const French     = 'fr_FR';
    public const Italian    = 'it_IT';
    public const Portuguese = 'pt_PT';

    /**
     * Converteix un codi ISO 639-3 a LanguageCode.
     *
     * @param string $code El codi en format ISO 639-3.
     *
     * @return string El codi de l'idioma.
     */
    public static function ISO639toLanguageCode(string $code): ?string
    {
        switch ($code)
        {
            case 'spa':
                return self::Spanish;
            case 'cat':
                return self::Catalan;
            case 'eng':
                return self::English;
            case 'fra':
                return self::French;
            case 'ita':
                return self::Italian;
            case 'por':
                return self::Portuguese;
        }
        return null;
    }

    /**
     * Coinverteix un LanguageCode a un codi ISO 639-3.
     *
     * @param string $iso El LanguageCode.
     *
     * @return string El codi de l'idioma en format ISO 639-3.
     */
    public static function languageCodeToISO639(string $iso): ?string
    {
        switch ($iso)
        {
            case self::Spanish:
                return 'spa';
            case self::Catalan:
                return 'cat';
            case self::English:
                return 'eng';
            case self::French:
                return 'fra';
            case self::Italian:
                return 'ita';
            case self::Portuguese:
                return 'por';
        }
        return null;
    }

    /**
     * Obté el millor (el més aproximat) LanguageCode per un locale concret.
     *
     * @param string $locale El locale a obtenir/convertir.
     * @param string|null $countryCode El codi del país.
     * @param string $default El locale per defecte quan sigui impossible determinar el locale.
     *
     * @return string L'idioma més aproximat a un locale.
     */
    public static function bestLanguageCodeForLocale(string $locale, ?string $countryCode, string $default = LanguageCode::English): string
    {
        if (static::validateValue($locale)) return $locale;
        // try to get the closest one using only the "lang code" without country
        $locale = explode('_', $locale, 2)[0];
        // filter locales to get similar ones and discard the non equal ones
        $filtered = array_values(preg_filter('/'.$locale.'_.+/', '$0', static::values()));
        $count = count($filtered);
        // only one result? then return it
        if ($count === 1) return $filtered[0];
        // more than one result?
        elseif ($count > 1)
        {
            // is a country specified?
            if ($countryCode && static::validateValue($locale.'_'.$countryCode))
            {
                return $locale.'_'.$countryCode;
            }
            // no matches, so get the default one for this language ()
            return $filtered[0];
        }
        else // count = 0, is a locale which has the country code?
        {
            $filtered = array_values(preg_filter('/.+_'.$countryCode.'/', '$0', static::values()));
            $count = count($filtered);
            // get the first one...
            if ($count > 0) return $filtered[0];
        }
        // no matches found, return the default language
        return $default;
    }

    /**
     * Retorna el nom de l'idioma en un idioma concret.
     *
     * @param string $code El codi d'idioma que es vol el nom.
     * @param string $locale L'idioma en el que es vol el nom.
     *
     * @return string El nom de l'idioma.
     */
    public static function localizedName(string $code, string $locale): string
    {
        // try to get the closest one using only the "lang code" without country
        $codeN = explode('_', $code, 2)[0];
        // filter locales to get similar ones and discard the non equal ones
        $filtered = array_values(preg_filter('/'.$codeN.'_.+/', '$0', static::values()));
        $count = count($filtered);
        // more than one (return it including the country)
        if ($count > 1) return mb_convert_case(Locale::getDisplayName($code, $locale), MB_CASE_TITLE, 'UTF-8');
        // localized name (only the language name) without country
        return mb_convert_case(Locale::getDisplayName($codeN, $locale), MB_CASE_TITLE, 'UTF-8');
    }
}
