<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Languages;

/**
 * Idiomes disponibles
 */
class Language
{
    /** @var string|null  */
    protected static $_locale = null;

    /**
     * Comprova que un codi d'idioma és vàlid.
     *
     * @param string $code
     *
     * @return bool TRUE = Si, FALSE = no.
     */
    public static function validCode(string $code): bool
    {
        return LanguageCode::validateValue($code);
    }

    /**
     * Configura quin és l'idioma global.
     *
     * @param string $locale El codi de l'idioma.
     */
    public static function setCurrentLanguage(string $locale): void
    {
        if ($locale !== self::$_locale && self::validCode($locale))
        {
            // remember this local code
            self::$_locale = $locale;
        }
    }

    /**
     * Obté l'idioma global actual.
     *
     * @param string|null $default L'idioma per defecte en el cas de que no estigui configurat.
     *
     * @return string El codi de l'idioma.
     */
    public static function currentLanguage(?string $default = LanguageCode::English): ?string
    {
        if ($default && ! self::$_locale) return $default;
        // the configured value
        return self::$_locale;
    }
}
