<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class TryingToDeleteResourceInUseException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_TRYING_DELETE_RESOURCE_IN_USE';

    protected $messages = [
        LanguageCode::English => 'The resource is being used and cannot be deleted.',
        LanguageCode::Spanish => 'El recurso se está usando y no se puede eliminar.',
        LanguageCode::Catalan => 'El recurs s\'està fent servir i no es pot eliminar.',
    ];
}
