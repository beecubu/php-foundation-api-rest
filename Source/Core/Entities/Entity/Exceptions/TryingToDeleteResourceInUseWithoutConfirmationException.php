<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class TryingToDeleteResourceInUseWithoutConfirmationException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_TRYING_DELETE_RESOURCE_IN_USE_WITHOUT_CONFIRMATION';

    protected $messages = [
        LanguageCode::English => 'The resource is being used and cannot be deleted without confirmation.',
        LanguageCode::Spanish => 'El recurso se está usando y no se puede eliminar sin confirmación.',
        LanguageCode::Catalan => 'El recurs s\'està fent servir i no es pot eliminar sense confirmació.',
    ];
}
