<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class InvalidIdentifierException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_INVALID_OBJECT_IDENTIFIER';

    protected $messages = [
        LanguageCode::English => 'The identifier specified is not valid.',
        LanguageCode::Spanish => 'El identificador especificado no es válido.',
        LanguageCode::Catalan => 'L\'identificador especificat no és vàlid.',
    ];
}
