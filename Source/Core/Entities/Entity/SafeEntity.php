<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Entity;

use Beecubu\Foundation\MongoDB\PlainEntity;
use stdClass;

class SafeEntity extends PlainEntity
{
    /**
     * Crea una nueva instancia de forma "segura".
     *
     * @param stdClass $data
     *
     * @return static
     */
    public static function create(stdClass $data): self
    {
        return static::instanceWithJson($data, false, true);
    }

    /**
     * Modifica los valors del objeto de forma "segura"
     *
     * @param stdClass $data La información a modificar.
     */
    public function edit(stdClass $data): void
    {
        $this->fromJson($data, false, true);
    }
}
