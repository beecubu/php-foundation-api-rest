<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Entity;

use Beecubu\Foundation\ApiRest\Api\Entities\Filter\Filter;
use Beecubu\Foundation\ApiRest\Api\Entities\Filter\FilterResult;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\InvalidIdentifierException;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\TryingToDeleteResourceInUseException;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\TryingToDeleteResourceInUseWithoutConfirmationException;
use Beecubu\Foundation\ApiRest\Core\Entities\Exceptions\RequiredPropertiesAreMissingException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\UnexpectedUserInstanceException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\OwnerUser;
use Beecubu\Foundation\ApiRest\Core\Persistence\EntityDB;
use Beecubu\Foundation\Core\Property;
use MongoDB\BSON\ObjectId;
use stdClass;

/**
 * @property-read string $id Id de l'objecte.
 * @property-read string $ownerId L'id del propietari.
 * @property-read int $index L'índex per ordenar.
 * @property bool $active TRUE = Actiu, FALSE = inactiu.
 *
 * @method bool isActive()
 */
abstract class EntityWithOwner extends Entity
{
    /**
     * @return EntityDB
     */
    abstract static protected function getDbClass();

    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'ownerId' => [Property::READ_ONLY, Property::IS_STRING],
            'index'   => [Property::READ_ONLY, Property::IS_INTEGER],
            'active'  => [Property::READ_WRITE, Property::IS_BOOLEAN],
        ];
    }

    protected function excludedJsonProperties(): void
    {
        parent::excludedJsonProperties();
        // exclude json properties
        $this->excJsonProp = array_merge($this->excJsonProp, [
            'ownerId',
        ]);
    }

    // Class constructor

    /**
     * @throws InvalidIdentifierException
     */
    protected static function instanceByRawDataId($id)
    {
        if ($id instanceof stdClass && isset($id->id))
        {
            return static::getDbClass()->get($id->id);
        }
        elseif (is_array($id) && isset($id['id']))
        {
            return static::getDbClass()->get($id['id']);
        }
        elseif (is_string($id) || $id instanceof ObjectId)
        {
            return static::getDbClass()->get($id);
        }
        throw new InvalidIdentifierException();
    }

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct();
        // set defaults
        $this->set_ivar('active', true);
    }

    /**
     * Crea un nuevo objeto y lo guarda a la DB.
     *
     * @param OwnerUser $ownerUser El propietario.
     * @param stdClass $data La información.
     *
     * @return static
     */
    public static function create(OwnerUser $ownerUser, stdClass $data): self
    {
        $obj = static::instanceWithJson($data, false, true);
        $obj->set_ivar('ownerId', $ownerUser->id);
        return $obj;
    }

    /**
     * Obtiene una instancia a partir de su Id.
     *
     * @param string $id El id del objeto.
     *
     * @return static|null
     *
     * @throws InvalidIdentifierException
     */
    public static function byId(string $id): ?self
    {
        return static::instanceByRawDataId($id);
    }

    /**
     * Obtiene todos los objetos disponibles.
     *
     * @param OwnerUser $ownerUser EL propietario de los objetos.
     *
     * @return static[]
     */
    public static function all(OwnerUser $ownerUser): array
    {
        return static::getDbClass()->all($ownerUser->id);
    }

    /**
     * Obtiene el total de objetos disponibles.
     *
     * @param OwnerUser $ownerUser El propietario de los objetos.
     *
     * @return int
     */
    public static function count(OwnerUser $ownerUser): int
    {
        return static::getDbClass()->count($ownerUser->id);
    }

    /**
     * Obtiene todos los objetos ACTIVOS disponibles.
     *
     * @param OwnerUser $ownerUser El propietario de los objetos.
     *
     * @return static[]
     */
    public static function allActive(OwnerUser $ownerUser): array
    {
        return static::getDbClass()->allActive($ownerUser->id);
    }

    /**
     * Oté el recompte d'objectes ACTIUS disponibles.
     *
     * @param OwnerUser $ownerUser
     *
     * @return int
     */
    public static function countActive(OwnerUser $ownerUser): int
    {
        return static::getDbClass()->countActive($ownerUser->id);
    }

    /**
     * Obté els objectes després d'aplicar un filtre.
     *
     * @return void
     */
    public static function filtered(OwnerUser $ownerUser, Filter $filter): FilterResult
    {
        return static::getDbClass()->filtered($ownerUser->id, $filter);
    }

    // Protected methods

    /**
     * Guarda los cambios.
     */
    protected function doSave(): void
    {
        static::getDbClass()->save($this);
    }

    /**
     * Elimina el objeto.
     *
     * @return bool
     */
    protected function doDelete(): bool
    {
        return static::getDbClass()->delete($this);
    }

    /**
     * Comprueba si está en uso.
     *
     * @return bool
     */
    protected function inUse(): bool
    {
        return static::getDbClass()->inUse($this);
    }

    /**
     * Devuelve si el objeto, se puede eliminar cuando está considerado en uso.
     *
     * @return bool
     */
    protected function canBeDeleteInUse(): bool
    {
        return true;
    }

    // Public methods

    /**
     * Guarda los cambios.
     *
     * @throws RequiredPropertiesAreMissingException
     */
    public function save(): void
    {
        $this->validate();
        $this->doSave();
    }

    /**
     * Elimina el objeto.
     *
     * @param bool $confirm TRUE = En el caso de que se esté usando lo eliminara, FALSE = generará error si se esta usando.
     *
     * @return bool
     *
     * @throws TryingToDeleteResourceInUseException
     * @throws TryingToDeleteResourceInUseWithoutConfirmationException
     */
    public function delete(bool $confirm): bool
    {
        if ($this->inUse())
        {
            if ( ! $this->canBeDeleteInUse())
            {
                throw new TryingToDeleteResourceInUseException();
            }
            else if ( ! $confirm)
            {
                throw new TryingToDeleteResourceInUseWithoutConfirmationException();
            }
        }
        // delete
        return $this->doDelete();
    }

    /**
     * Comprueba que un usuario sea propietario del objeto.
     *
     * @param OwnerUser|null $ownerUser El propietario a comprobar.
     *
     * @return bool
     */
    public function ensureOwnerUserCanManageThis(?OwnerUser $ownerUser): bool
    {
        return $ownerUser && $this->ownerId === $ownerUser->id;
    }

    /**
     * El propietario.
     *
     * @return OwnerUser
     *
     * @throws UnexpectedUserInstanceException
     */
    public function getOwner(): OwnerUser
    {
        return OwnerUser::userById($this->ownerId);
    }
}
