<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Entity;

use Beecubu\Foundation\ApiRest\Core\Entities\Exceptions\RequiredPropertiesAreMissingException;

class Entity extends \Beecubu\Foundation\MongoDB\Entity
{
    /** @var string[] $requiredProperties */
    protected $requiredProperties;

    /**
     * Entity constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // some inits
        $this->initRequiredProperties();
    }

    /**
     * Define las propiedades obligatorias.
     */
    protected function initRequiredProperties(): void
    {
        $this->requiredProperties = [];
    }

    /**
     * Comprueba que todas las propiedades importantes estén asignadas.
     *
     * @param string $callerProperty El property ques se está analizando.
     *
     * @return string[]
     */
    protected function verifyRequiredProperties(string $callerProperty): array
    {
        $errors = [];
        // verify the required properties
        foreach (array_keys($this->properties) as $property)
        {
            $value = null;
            // is a "write-only" property? then get the ivar
            if ($this->isWriteOnlyProperty($property))
            {
                $value = $this->ivars[$property] ?? null;
            }
            else // get the property value
            {
                $value = $this->$property;
            }
            // is this value assigned?
            if ($value !== null)
            {
                if ($value instanceof Entity)
                {
                    $errors = array_merge($errors, $value->verifyRequiredProperties($callerProperty.'.'.$property));
                }
                elseif (is_array($value))
                {
                    foreach ($value as $index => $item)
                    {
                        if ($item instanceof Entity)
                        {
                            $errors = array_merge($errors, $item->verifyRequiredProperties($callerProperty.'.'.$property."[$index]"));
                        }
                    }
                }
            }
            else // is null, so... ops!!
            {
                if (in_array($property, $this->requiredProperties))
                {
                    $errors[] = $callerProperty !== '' ? $callerProperty.'.'.$property : $property;
                }
            }
        }
        // the errors
        return $errors;
    }

    /**
     * Valida los datos.
     *
     * @throws RequiredPropertiesAreMissingException
     */
    protected function validate(): void
    {
        if ($errors = $this->verifyRequiredProperties('') and count($errors) > 0)
        {
            throw new RequiredPropertiesAreMissingException($errors);
        }
    }

    /**
     * Acciones a realizar antes de eliminar el objeto.
     */
    public function afterDelete(): void
    {
        // Implement this in child classes...
    }
}
