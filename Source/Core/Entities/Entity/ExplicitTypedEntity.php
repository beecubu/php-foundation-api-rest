<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Entity;

use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\MongoDB\ExplicitEntity;
use function Beecubu\Foundation\Helpers\Arrays\valueFromArrayOrStdClass as getValue;

/**
 * Un Entity amb "type".
 *
 * @property-read string $type El tipus.
 */
class ExplicitTypedEntity extends ExplicitEntity
{
    protected const TYPE = null;

    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'type' => [Property::READ_ONLY, Property::IS_STRING], // pre-calculated
        ];
    }

    protected function getType(): string
    {
        return static::TYPE;
    }

    /**
     * Retorna el "::class" segons el type.
     *
     * @param string $type El type a obtenir el class.
     *
     * @return string|null
     */
    protected static function classForType(string $type)
    {
        return null;
    }

    /** @inheritdoc */
    public static function instanceWithJson($json, bool $replace = true, bool $writableOnly = false)
    {
        $class = static::classForType(getValue($json, 'type'));
        $obj = new $class();
        $obj->fromJson($json, $replace, $writableOnly);
        return $obj;
    }
}
