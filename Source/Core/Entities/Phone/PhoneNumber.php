<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Phone;

use Beecubu\Foundation\ApiRest\Core\Entities\Phone\Exceptions\PhoneNumberIsNotValidException;
use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\MongoDB\PlainEntity;
use Exception;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberType;
use libphonenumber\PhoneNumberUtil;
use function Beecubu\Foundation\Helpers\Phone\phoneNumberFormatter;
use function Beecubu\Foundation\Helpers\Phone\phoneNumberParser;
use function Beecubu\Foundation\Helpers\String\parseInt;

defined('PHONE_DEFAULT_REGION') or define('PHONE_DEFAULT_REGION', 'ES');

/**
 * Define un numero de teléfono.
 *
 * @property-read string $code El código de el país.
 * @property-read string $number El número de teléfono.
 */
class PhoneNumber extends PlainEntity
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'code'   => [Property::READ_ONLY, Property::IS_STRING],
            'number' => [Property::READ_ONLY, Property::IS_STRING],
        ];
    }

    protected function setCode(?string $value): void
    {
        $this->set_ivar('code', $value ? parseInt($value) : null);
    }

    protected function setNumber(?string $value): void
    {
        $this->set_ivar('number', $value ? parseInt($value) : null);
    }

    // Class constructor

    /**
     * Crea un nou número de telèfon.
     *
     * @param string $code El codi del país.
     * @param string $number El número de telèfon.
     *
     * @return static El número de telèfon.
     */
    public static function create(string $code, string $number): self
    {
        $obj = new static();
        $obj->setCode($code);
        $obj->setNumber($number);
        return $obj;
    }

    /**
     * Crea un nou número de telèfon a partir d'un string.
     *
     * @param string $phone El número de telèfon a analitzar.
     *
     * @return static El número de telèfon.
     *
     * @throws PhoneNumberIsNotValidException
     */
    public static function parse(string $phone): self
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();
        // get the phone object
        try
        {
            $phoneNumberObject = $phoneNumberUtil->parse($phone, PHONE_DEFAULT_REGION);
            // return the new phone number
            $instance = static::create((string)$phoneNumberObject->getCountryCode(), $phoneNumberObject->getNationalNumber() ?? '');
            // is not a valid phone number
            if ( ! $instance->isValid()) throw new Exception();
            // all done
            return $instance;
        }
        catch (Exception $exception)
        {
            throw new PhoneNumberIsNotValidException($phone);
        }
    }

    // Public methods

    protected function importProperties($data, bool $replace = true, bool $rawDataMode = false, bool $writableOnly = false): void
    {
        if (is_string($data) && ! empty($data))
        {
            $this->ivars = self::parse($data)->ivars;
        }
        else // import properties using the regular method
        {
            parent::importProperties($data, $replace, $rawDataMode, $writableOnly);
        }
    }

    /**
     * Retorna el número de telèfon amb el codi de país.
     *
     * @param string $separator El separador a fer servir entre el codi i el numero.
     *
     * @return string El número de telèfon per ús internacional.
     */
    public function getFullPhone(string $separator = ''): string
    {
        return $this->code.$separator.$this->number;
    }

    /**
     * Retorna el número de telèfon en format numèric (int).
     *
     * @return int El número de telèfon.
     */
    public function getFullPhoneAsInt(): int
    {
        return (int)$this->getFullPhone();
    }

    /**
     * Retorna el número de telèfon amb el codi de país i en un format bonic.
     *
     * @return string El número de telèfon bonic.
     */
    public function getFullPhoneFormatted(): string
    {
        return phoneNumberFormatter($this->number ?? '', $this->code ?? '');
    }

    /**
     * Retorna el número de telèfon parcialment ocult.
     *
     * @return string El número de telèfon.
     */
    public function getHiddenFullPhoneFormatted(): string
    {
        $phone = phoneNumberParser($this->number);
        // hide phone number
        $phone = str_pad(substr($phone, -3), strlen($phone), '*', STR_PAD_LEFT);
        // has a country code?
        return "(+$this->code) ".strrev(implode(' ', str_split(strrev($phone), 3)));
    }

    /**
     * Comprova que sigui un numero de telèfon vàlid.
     *
     * @return bool TRUE = Ho és, FALSE = doncs no.
     */
    public function isValid(): bool
    {
        try
        {
            $phoneNumberUtil = PhoneNumberUtil::getInstance();
            // get the phone object
            $phoneNumberObject = $phoneNumberUtil->parse('+'.$this->getFullPhone());
            // is a possible Mobile phone number?
            return $phoneNumberUtil->isValidNumber($phoneNumberObject);
        }
        catch (NumberParseException $e)
        {
            return false;
        }
    }

    /**
     * Comprova que "sigui" un telèfon mòbil.
     *
     * @return bool TRUE = Ho és, FALSE = doncs no.
     */
    public function isValidMobileNumber(): bool
    {
        try
        {
            $phoneNumberUtil = PhoneNumberUtil::getInstance();
            // get the phone object
            $phoneNumberObject = $phoneNumberUtil->parse('+'.$this->getFullPhone());
            // get the number type
            $type = $phoneNumberUtil->getNumberType($phoneNumberObject);
            // is a possible Mobile phone number?
            return $type === PhoneNumberType::MOBILE || $type === PhoneNumberType::FIXED_LINE_OR_MOBILE;
        }
        catch (NumberParseException $e)
        {
            return false;
        }
    }

    public function __toString(): string
    {
        return $this->getFullPhoneFormatted();
    }
}
