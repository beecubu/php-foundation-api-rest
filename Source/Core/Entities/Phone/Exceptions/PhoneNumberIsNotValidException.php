<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Phone\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan el número de telèfon no és correcte.
 */
class PhoneNumberIsNotValidException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_PHONE_NUMBER_IS_NOT_VALID';

    protected $messages = [
        LanguageCode::English => 'The phone number "%s" is not valid.',
        LanguageCode::Spanish => 'El numero de telefono "%s" no es válido.',
        LanguageCode::Catalan => 'El número de telèfon "%s" no és vàlid.',
    ];

    /**
     * PhoneNumberIsNotValidException constructor.
     *
     * @param string $phone
     */
    public function __construct(string $phone)
    {
        parent::__construct();
        // update messages
        $this->messages[LanguageCode::English] = sprintf($this->messages[LanguageCode::English], $phone);
        $this->messages[LanguageCode::Spanish] = sprintf($this->messages[LanguageCode::Spanish], $phone);
        $this->messages[LanguageCode::Catalan] = sprintf($this->messages[LanguageCode::Catalan], $phone);
    }
}
