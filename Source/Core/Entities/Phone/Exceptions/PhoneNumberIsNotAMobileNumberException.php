<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Phone\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;
use Beecubu\Foundation\ApiRest\Core\Entities\Phone\PhoneNumber;

/**
 * Quan el número de telèfon no és d'un mòbil.
 */
class PhoneNumberIsNotAMobileNumberException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_PHONE_NUMBER_IS_NOT_MOBILE_PHONE';

    protected $messages = [
        LanguageCode::English => 'The phone number "%s" is not a mobile phone number.',
        LanguageCode::Spanish => 'El numero de telefono "%s" no es un número de teléfono mobil.',
        LanguageCode::Catalan => 'El número de telèfon "%s" no és un número de telèfon mòvil.',
    ];

    /**
     * PhoneNumberIsNotValidException constructor.
     *
     * @param PhoneNumber $phone
     */
    public function __construct(PhoneNumber $phone)
    {
        parent::__construct();
        // update messages
        $this->messages[LanguageCode::English] = sprintf($this->messages[LanguageCode::English], $phone->getFullPhoneFormatted());
        $this->messages[LanguageCode::Spanish] = sprintf($this->messages[LanguageCode::Spanish], $phone->getFullPhoneFormatted());
        $this->messages[LanguageCode::Catalan] = sprintf($this->messages[LanguageCode::Catalan], $phone->getFullPhoneFormatted());
    }
}
