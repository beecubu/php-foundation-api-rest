<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\LocalizedString;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\Language;
use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\MongoDB\PlainEntity;
use stdClass;

/**
 * Defineix l'estructura d'una empresa
 *
 * @property-read string $default El text en l'idioma configurat com a "per defecte".
 * @property-read array<string, string> $translations Els texts en els idiomes disponibles.
 */
class LocalizedString extends PlainEntity
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'default'      => [Property::READ_ONLY, Property::IS_STRING], // pre-calculated
            'translations' => [Property::READ_ONLY, Property::IS_DICTIONARY],
        ];
    }

    // Overridden setters/getters

    protected function getDefault(): string
    {
        return $this->getString(Language::currentLanguage() ?? LanguageCode::English);
    }

    // Overridden methods

    /**
     * Crea una nova instància de LocalizedString.
     *
     * @param array $strings Array amb els codis i el seu text corresponent.
     *
     * @return LocalizedString
     */
    public static function create(array $strings): LocalizedString
    {
        $localizedString = new LocalizedString();
        $localizedString->setStrings($strings);
        return $localizedString;
    }

    // Public methods

    public function __toString(): string
    {
        return $this->default;
    }

    /**
     * Modifica/Afegeix una traducció del text.
     *
     * @param string $langCode El codi d'idioma (ca, en, es...)
     * @param string $string El text en l'idioma introduït.
     */
    public function setString(string $langCode, string $string): void
    {
        $this->ivars['translations'][$langCode] = $string;
    }

    /**
     * Modifica/Afegeix un conjunt de traduccions del text.
     *
     * @param array $strings Array amb els codis i el seu text corresponent.
     * @param bool $keepOriginals TRUE = No substitueix els valors ja existents, FALSE = Substitueix tots els valors.
     */
    public function setStrings(array $strings, bool $keepOriginals = true): void
    {
        if ( ! $keepOriginals)
        {
            $this->clear();
        }
        // add new ones
        foreach ($strings as $key => $value)
        {
            if ( ! $keepOriginals || ! isset($this->translations[$key]))
            {
                $this->setString($key, $value);
            }
        }
    }

    /**
     * Obté la traducció en un idioma concret del text.
     *
     * @param string $langCode El codi d'idioma (ca, en, es...)
     *
     * @return string
     */
    public function getString(string $langCode): string
    {
        if (isset($this->translations[$langCode]))
        {
            return $this->translations[$langCode];
        }
        elseif (count($this->translations) > 0)
        {
            return current($this->ivars['translations']);
        }
        return '';
    }

    /**
     * Elimina totes les traduccions del text.
     */
    public function clear(): void
    {
        $this->set_ivar('translations', []);
    }

    /**
     * Elimina un idioma de la llista.
     *
     * @param string $langCode El codi d'idioma (ca, en, es...)
     */
    public function removeString(string $langCode): void
    {
        unset($this->translations[$langCode]);
    }

    /**
     * Retorna un llistat amb els idiomes que hi ha disponibles a l'string.
     *
     * @return array
     */
    public function availableLanguages(): array
    {
        return array_keys($this->translations);
    }

    /**
     * Retorna la informació del LocalizedString en format JSON.
     *
     * @param array|null $fields Els fields a ignorar o incloure.
     * @param bool $excluding TRUE = Estem ignorant els fields, FALSE = estem filtrant els fields.
     *
     * @return stdClass L'objecte en format json.
     */
    public function json(?array $fields = null, bool $excluding = true): stdClass
    {
        $json = new stdClass();
        foreach ($this->translations as $key => $value)
        {
            $json->$key = $value;
        }
        // return the json
        return $json;
    }
}
