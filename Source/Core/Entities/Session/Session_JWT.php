<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Session;

use Beecubu\Foundation\ApiRest\Core\Entities\Environment\Environment;
use Beecubu\Foundation\ApiRest\Core\Entities\Languages\Language;
use Beecubu\Foundation\Core\Exceptions\SerializeIsNotSubclassOfSerializableException;
use Delight\Cookie\Cookie;
use Exception;
use Jose\Component\Core\JWK;
use Jose\Easy\Build;
use Jose\Easy\Load;
use stdClass;
use function Beecubu\Foundation\Helpers\Id\uuid;

/**
 * Implementa una sessió utilitzant el PHP_session.
 */
abstract class Session_JWT extends Session
{
    const JWT_COOKIE_1 = null;
    const JWT_COOKIE_2 = null;

    const JWT_EXPIRE_TIME = 3600*48; // 48 hours

    const JWT_KEY = '.dsf@8-sdf.ds%9.!f7.sdf9eo\w403#';

    /**
     * Retorna la sessió actual.
     *
     * @return static La sessió actual.
     *
     * @throws Exception
     */
    public static function current()
    {
        return static::instanceFromCookies(parent::current());
    }

    /**
     * Inicia el objeto con los datos del JWT (cookies).
     *
     * @param Session_JWT|null $instance
     *
     * @return Session_JWT|null
     */
    protected static function instanceFromCookies(?Session_JWT $instance = null): ?self
    {
        if ( ! $instance) $instance = new static();
        // are the both cookies present?
        if (Cookie::exists(static::JWT_COOKIE_1) && Cookie::exists(static::JWT_COOKIE_2))
        {
            // get the complete token (public + private)
            $token = Cookie::get(static::JWT_COOKIE_1).'.'.Cookie::get(static::JWT_COOKIE_2);
            try
            {
                // validate the jwt
                $jwt = Load::jws($token)->exp()->iat(1000)->nbf()->key(self::jwk())->run();
                // get the session information
                $session = $jwt->claims->get('session');
                // repopulate session
                $instance->fromJson($session, true, true);
                // extend session
                $instance->save();
                // update current locale
                Language::setCurrentLanguage($session->locale ?? SESSION_DEFAULT_LANGUAGE);
            }
            catch (Exception $exception)
            {
                // invalid token, NO SESSION assumed...
            }
        }
        return $instance;
    }

    protected static function jwk(): JWK
    {
        static $jwk = null;
        if ( ! $jwk)
        {
            $jwk = new JWK([
                'kty' => 'oct',
                'k'   => base64_encode(static::JWT_KEY),
            ]);
        }
        return $jwk;
    }

    /**
     * Crea la cookie que conté la part "pública" del JWT (header.payload).
     *
     * @param string $value El header + payload
     *
     * @return Cookie La cookie.
     */
    private function publicCookie(string $value): Cookie
    {
        $cookie = new Cookie(static::JWT_COOKIE_1);
        $cookie->setSecureOnly(Environment::current()->isLiveMode());
        $cookie->setHttpOnly(false);
        $cookie->setValue($value);
        $cookie->setDomain('.'.Environment::current()->subdomain());
        $cookie->setExpiryTime(time() + static::JWT_EXPIRE_TIME);
        // the cookie
        return $cookie;
    }

    /**
     * Crea la cookie que conté la part "privada" del JWT (signature).
     *
     * @param string $value La signatura.
     *
     * @return Cookie La cookie.
     */
    private function privateCookie(string $value): Cookie
    {
        $cookie = new Cookie(static::JWT_COOKIE_2);
        $cookie->setSecureOnly(Environment::current()->isLiveMode());
        $cookie->setHttpOnly(true);
        $cookie->setValue($value);
        $cookie->setDomain('.'.Environment::current()->subdomain());
        $cookie->setExpiryTime(time() + static::JWT_EXPIRE_TIME);
        // the cookie
        return $cookie;
    }

    /**
     * Genera el JWT.
     *
     * @param stdClass $claim
     *
     * @throws Exception
     */
    protected function generateJWT(stdClass $claim): void
    {
        $time = time();
        // create the jwt token
        $token = Build::jws()
            ->exp($time + static::JWT_EXPIRE_TIME)->iat($time)->nbf($time)->jti(uuid(), true)->alg('HS256')
            ->claim('session', $claim)
            ->sign(self::jwk());
        // split token in three parts
        $tokenParts = explode('.', $token);
        // create the 'header.payload' cookie
        $this->publicCookie($tokenParts[0].'.'.$tokenParts[1])->save();
        // create the 'signature' cookie
        $this->privateCookie($tokenParts[2])->save();
    }

    /**
     * @inheritDoc
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     * @throws Exception
     */
    public function save(): void
    {
        if ($this->hasUser())
        {
            // the session
            $session = $this->json(['user']);
            $session->name = $this->user->name;
            // generate jwt
            $this->generateJWT($session);
        }
        else // no user, so delete session
        {
            $this->delete();
        }
    }

    /**
     * @inheritDoc
     */
    public function delete(): void
    {
        // public cookie
        if ($value = Cookie::get(static::JWT_COOKIE_1))
        {
            $this->publicCookie($value)->delete();
        }
        // private cookie
        if ($value = Cookie::get(static::JWT_COOKIE_2))
        {
            $this->privateCookie($value)->delete();
        }
        // clean up ivars
        $this->ivars = [];
    }
}
