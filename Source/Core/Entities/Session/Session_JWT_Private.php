<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Session;

/**
 * Implementa una sessió JWT per a usuaris privats utilitzant el PHP_session.
 */
class Session_JWT_Private extends Session_JWT
{
    const JWT_COOKIE_1 = '_pr1';
    const JWT_COOKIE_2 = '_pr2';

    const JWT_EXPIRE_TIME = 3600*(24*7); // 7 days
}
