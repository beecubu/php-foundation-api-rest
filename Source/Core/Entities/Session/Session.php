<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Session;

use Beecubu\Foundation\ApiRest\Core\Entities\Environment\Environment;
use Beecubu\Foundation\ApiRest\Core\Entities\Languages\Language;
use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\UnexpectedUserInstanceException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\User;
use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\Core\Serializable;

define('SESSION_NAME', 'SESSION');
define('SESSION_DEFAULT_LANGUAGE', LanguageCode::Catalan);

/**
 * Gestiona las sesiones.
 *
 * @property-read string $locale L'idioma configurat per aquesta sessió.
 * @property string $userId L'id de l'usuari de la sessió.
 * @property-read User $user El usuario en sesión.
 *
 * @method boolean hasLocale()
 * @method boolean hasUserId()
 * @method boolean hasUser()
 */
abstract class Session extends Serializable
{
    /** @var array|null  */
    protected $data = null;

    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'locale' => [Property::READ_ONLY, Property::IS_STRING],
            'userId' => [Property::READ_WRITE, Property::IS_STRING_ENCRYPTED],
            'user'   => [Property::READ_ONLY, Property::IS_OBJECT_NULLABLE, User::class],
        ];
    }

    // Overridden properties

    protected function getLocale(): string
    {
        if ( ! Language::currentLanguage(null))
        {
            if ($this->hasUser() && $this->user->hasLanguage())
            {
                Language::setCurrentLanguage($this->user->language);
            }
        }
        // the current locale
        return Language::currentLanguage();
    }

    /**
     * @throws UnexpectedUserInstanceException
     */
    protected function getUser(): ?User
    {
        if ( ! isset($this->ivars['user']))
        {
            if ($this->hasUserId() && $user = User::userById($this->userId))
            {
                $this->set_ivar('user', $user);
            }
            else // no userId!
            {
                $this->set_ivar('user', null);
            }
        }
        return $this->ivars['user'];
    }

    // Class constructors

    /**
     * Retorna la sessió actual.
     *
     * @return static La sessió actual.
     */
    public static function current()
    {
        static $instance = null;
        // first time?
        if ( ! $instance instanceof static)
        {
            // init the session object
            $instance = new static();
            // set up the default current language (if not login is done)
            Language::setCurrentLanguage(Environment::current()->locale());
        }
        // the new one
        return $instance;
    }

    /**
     * Guarda la sessió.
     */
    abstract public function save(): void;

    /**
     * Elimina la sessió.
     */
    abstract public function delete(): void;

    /**
     * Afegeix una variable "one-shot".
     *
     * @param string $key El nom de la variable a guardar.
     * @param mixed $value El valor a guardar.
     */
    public function setFlashValue(string $key, $value): void
    {
        $this->data[$key] = $value;
        // save session
        $this->save();
    }

    /**
     * Obté una variable "one-shot", després ja no existirà més!
     *
     * @param string $key La clau de la variable a obtenir.
     *
     * @return mixed El valor demanat.
     */
    public function getFlashValue(string $key)
    {
        if ($value = $this->data[$key] ?? null)
        {
            // clean-up the variable
            unset($this->data[$key]);
            // save session
            $this->save();
        }
        // the desired value
        return $value;
    }

    /**
     * Comprova si hi ha un usuari logged-in.
     *
     * @return bool TRUE = Si que n'hi ha un, FALSE = pos no.
     */
    public function isLogged(): bool
    {
        return $this->hasUserId();
    }
}
