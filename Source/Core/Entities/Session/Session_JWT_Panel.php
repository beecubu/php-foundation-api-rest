<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Session;

/**
 * Implementa una sessió JWT per al panell utilitzant el PHP_session.
 */
class Session_JWT_Panel extends Session_JWT
{
    const JWT_COOKIE_1 = '_pa1';
    const JWT_COOKIE_2 = '_pa2';

    const JWT_EXPIRE_TIME = 3600*48; // 48 hours
}
