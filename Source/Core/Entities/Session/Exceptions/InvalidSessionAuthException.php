<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan no hi ha cap sessió activa.
 */
class InvalidSessionAuthException extends LocalizedException
{
    /** @var string $code  */
    protected $code = 'ERR_INVALID_SESSION_AUTH';

    protected $messages = [
        LanguageCode::English => 'The session auth is not valid.',
        LanguageCode::Spanish => 'Las credenciales de la sesión son incorrectas.',
        LanguageCode::Catalan => 'Les credencials de la sessió són incorrectes.',
    ];
}
