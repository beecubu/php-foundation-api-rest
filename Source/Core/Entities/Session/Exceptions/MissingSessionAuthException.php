<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan no hi ha cap sessió activa.
 */
class MissingSessionAuthException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_MISSING_SESSION_AUTH';

    protected $messages = [
        LanguageCode::English => 'The session auth is missing.',
        LanguageCode::Spanish => 'Faltan las credenciales de la sesión.',
        LanguageCode::Catalan => 'Falten les credencials de la sessió.',
    ];
}
