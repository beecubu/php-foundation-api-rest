<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan un usuari no té accés a la plataforma.
 */
class SessionForbiddenAccessException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_FORBIDDEN_ACCESS';

    protected $messages = [
        LanguageCode::English => 'Forbidden access. Please contact us.',
        LanguageCode::Spanish => 'Acceso no autorizado. Por favor, ponte en contacto con nosotros.',
        LanguageCode::Catalan => 'Accés no autoritzat. Si us plau, posa\'t en contacte amb nosaltres.',
    ];
}
