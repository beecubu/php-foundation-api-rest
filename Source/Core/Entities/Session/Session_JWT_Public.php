<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Session;

/**
 * Implementa una sessió JWT per a usuaris públics utilitzant el PHP_session.
 */
class Session_JWT_Public extends Session_JWT
{
    const JWT_COOKIE_1 = '_pu1';
    const JWT_COOKIE_2 = '_pu2';

    const JWT_EXPIRE_TIME = 3600*(24*15); // 15 days
}
