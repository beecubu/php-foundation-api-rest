<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Environment;

use Beecubu\Foundation\ApiRest\Core\Entities\IPInfo\IPInfo;
use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Locale;

defined('ENVIRONMENT_DOCKER_FLAG') or define('ENVIRONMENT_DOCKER_FLAG', 'RUNNING_IN_DOCKER');

/**
 * Obté informació de l'entorn.
 */
class Environment
{
    /** @var bool|null */
    private $ivar_debug = null;
    /** @var bool|null */
    private $ivar_sandbox = null;

    /**
     * Retorna la instància actual.
     *
     * @return Environment La instància actual
     */
    public static function current(): self
    {
        static $instance = null;
        // create a new instance
        if ( ! $instance) $instance = new static();
        // return the instance
        return $instance;
    }

    /**
     * Retorna el mètode HTTP o HTTPS.
     *
     * @return string El mètode http.
     */
    private function httpMethod(): string
    {
        if ($this->isLocalhost()) return 'http';
        // https always...
        return 'https';
    }

    /**
     * Activa o desactiva el mode debug.
     *
     * @param bool $debug TRUE = Debug ON, FALSE = Debug FALSE.
     */
    public function setDebugMode(bool $debug): void
    {
        $this->ivar_debug = $debug;
    }

    /**
     * Activa o desactiva el mode sandbox (pagaments).
     *
     * @param bool $sandbox TRUE = Sandbox ON, FALSE = Sandbox OFF.
     */
    public function setSandboxMode(bool $sandbox): void
    {
        $this->ivar_sandbox = $sandbox;
    }

    /**
     * Comprova si la web està corrent en localhost.
     *
     * @return bool TRUE = En localhost, FALSE = nope.
     */
    public function isLocalhost(): bool
    {
        if (in_array($this->host(), ['localhost', '127.0.0.1']))
        {
            return true;
        }
        return false;
    }

    /**
     * Comprueba si la web está corriendo en el servidor final (producción).
     *
     * @return bool
     */
    public function isProductionServer(): bool
    {
        return ! $this->isLocalhost();
    }

    /**
     * Retorna si la web està corrent en modo debug.
     *
     * @return bool TRUE = En mode debug, FALSE = nope.
     */
    public function isDebugMode(): bool
    {
        if ($this->ivar_debug !== null) return $this->ivar_debug;
        // auto-determine the debug mode
        if ($this->isLocalhost())
        {
            return true;
        }
        elseif (strpos($this->host(), 'debug.') === 0)
        {
            return true;
        }
        elseif (strpos($this->host(), '.localhost') !== false)
        {
            return true;
        }
        elseif (strpos($this->host(), '.localtunnel.me') !== false)
        {
            return true;
        }
        elseif (strpos($this->host(), '.ngrok.io') !== false)
        {
            return true;
        }
        // live mode
        return false;
    }

    /**
     * Retorna si els pagaments de la web estan corrent en modo sandbox.
     *
     * @return bool TRUE = En modo sandbox, FALSE = nope.
     */
    public function isSandboxMode(): bool
    {
        if ($this->ivar_sandbox !== null) return $this->ivar_sandbox;
        // auto-determine the sandbox mode
        return false;
    }

    /**
     * Retorna si la web està corrent en modo Live.
     *
     * @return bool TRUE = En modo live, FALSE = nope.
     */
    public function isLiveMode(): bool
    {
        return ! $this->isDebugMode() && ! $this->isSandboxMode();
    }

    /**
     * Devuelve si se está accediendo desde la versión App.
     *
     * @return bool
     */
    public function isAppMode(): bool
    {
        return $this->getOrigin() === 'capacitor://localhost';
    }

    /**
     * Comprova si s'està executant des de consola (Cli).
     *
     * @return bool
     */
    public function isRunningFromCli(): bool
    {
        return php_sapi_name() === 'cli';
    }

    /**
     * Comprova si s'està executant des de dins d'un Docker.
     *
     * @return bool
     */
    public function isRunningInDocker(): bool
    {
        return ($_SERVER[ENVIRONMENT_DOCKER_FLAG] ?? false) === '1';
    }

    /**
     * Retorna el host del servidor actual.
     *
     * @return string El host del servidor actual.
     */
    public function host(): string
    {
        return $_SERVER['HTTP_HOST'] ?? 'localhost';
    }

    /**
     * Retorna el subdomini del servidor actual.
     *
     * @return string El subdomini del servidor actual.
     */
    public function subdomain(): string
    {
        if ($parts = explode('.', $this->host(), 2))
        {
            return $parts[0];
        }
        return $this->host();
    }

    /**
     * Retorna el domini del servidor actual.
     *
     * @return string El domini del servidor actual.
     */
    public function domain(): string
    {
        if ($parts = explode('.', $this->host(), 2))
        {
            return count($parts) > 1 ? $parts[1] : $parts[0];
        }
        return $this->host();
    }

    /**
     * Retorna l'idioma a utilitzar segons l'usuari.
     *
     * @param string $default L'idioma per defecte en el cas de que no es pugui determinar.
     *
     * @return string El codi d'idioma.
     */
    public function locale(string $default = LanguageCode::English): string
    {
        // get the user-agent locale
        $locale = Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE'] ?? $default);
        // get the best available locale for this locale
        return LanguageCode::bestLanguageCodeForLocale($locale, IPInfo::get()->country, $default);
    }

    /**
     * Retorna la URL actual.
     *
     * @return string
     */
    public function baseUrl(): string
    {
        static $baseUrl = null;
        // generate the base url
        if ( ! $baseUrl) $baseUrl = $this->httpMethod().'://'.$this->host().'/';
        // the url
        return $baseUrl;
    }

    /**
     * Retorna la URL actual del domini actual.
     *
     * @return string
     */
    public function baseDomainUrl(): string
    {
        static $baseUrl = null;
        // generate the base url
        if ( ! $baseUrl) $baseUrl = $this->httpMethod().'://www.'.$this->domain().'/';
        // the url
        return $baseUrl;
    }

    /**
     * Retorna la URL d'un subdomini concret.
     *
     * @param string $subdomain El subdomini a generar.
     *
     * @return string El subdomini generat.
     */
    public function baseSubdomainUrl(string $subdomain): string
    {
        static $baseUrl = null;
        // generate the base url
        if ( ! $baseUrl) $baseUrl = $this->httpMethod().'://%s.'.$this->domain().'/';
        // the url
        return sprintf($baseUrl, $subdomain);
    }

    /**
     * @return bool
     */
    public function hasReferer(): bool
    {
        return isset($_SERVER['HTTP_REFERER']);
    }

    /**
     * Obté el referrer.
     *
     * @return string El referrer.
     */
    public function getReferer(bool $baseUrlFallback): string
    {
        if ($this->hasReferer()) return $_SERVER['HTTP_REFERER'];
        // use the "baseUrl" as referer
        if ($baseUrlFallback) return $this->baseUrl();
        // "default" referer
        return '/';
    }

    /**
     * Obté l'Origin.
     *
     * @return string
     */
    public function getOrigin(): string
    {
        return $_SERVER['HTTP_ORIGIN'] ?? '';
    }
}
