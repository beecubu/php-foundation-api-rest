<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan s'intenta eliminar l'usuari en sessió.
 */
class TryingToDeleteTheLoggedUserException extends LocalizedException
{
    protected $code = 'ERR_TRYING_DELETE_LOGGED_USER';

    protected $messages = [
        LanguageCode::English => 'Is not possible to delete yourself.',
        LanguageCode::Spanish => 'No puedes eliminarte tu mismo.',
        LanguageCode::Catalan => 'No et pots el·liminar tu mateix.',
    ];
}
