<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * La contraseña ya está en uso.
 */
class EmailAlreadyInUseException extends LocalizedException
{
    protected $code = 'ERR_EMAIL_ALREADY_IN_USE';

    protected $messages = [
        LanguageCode::English => 'The email is already in use.',
        LanguageCode::Spanish => 'Este correo ya se está en uso.',
        LanguageCode::Catalan => 'Aquest correu ja s\'està fent servir.',
    ];
}
