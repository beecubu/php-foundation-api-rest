<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan s'intenta suspendre a l'usuari actiu.
 */
class TryingToSuspendLoggedUserException extends LocalizedException
{
    protected $code = 'ERR_TRYING_SUSPEND_LOGGED_USER';

    protected $messages = [
        LanguageCode::English => 'Trying to suspend yourself.',
        LanguageCode::Spanish => 'No puedes suspenderte a ti mismo.',
        LanguageCode::Catalan => 'No et pots suspendre a tu mateix.',
    ];
}
