<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan la contrasenya és massa curta.
 */
class PasswordIsTooShortException extends LocalizedException
{
    protected $code = 'ERR_PASSWORD_IS_TOO_SHORT';

    protected $messages = [
        LanguageCode::English => 'The minimum password length is 6 characters.',
        LanguageCode::Spanish => 'La longitud mínima de la contraseña es de 6 caracteres.',
        LanguageCode::Catalan => 'La longitud mínima de la contrasenya és de 6 caràcters.',
    ];
}
