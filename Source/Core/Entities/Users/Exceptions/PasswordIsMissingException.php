<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan falta la contrasenya.
 */
class PasswordIsMissingException extends LocalizedException
{
    protected $code = 'ERR_PASSWORD_IS_MISSING';

    protected $messages = [
        LanguageCode::English => 'The password is missing.',
        LanguageCode::Spanish => 'Falta la contraseña.',
        LanguageCode::Catalan => 'Falta la contrasenya.',
    ];
}
