<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan s'intenta fer un down-grade de l'usuari en sessió..
 */
class TryingToDowngradeLoggedUserException extends LocalizedException
{
    protected $code = 'ERR_TRYING_DOWNGRADE_LOGGED_USER';

    protected $messages = [
        LanguageCode::English => 'Trying to downgrade yourself.',
        LanguageCode::Spanish => 'No puedes quitarte el nivel de Admin tu mismo.',
        LanguageCode::Catalan => 'No et pots treure el nivell d\'Admin tu mateix.',
    ];
}
