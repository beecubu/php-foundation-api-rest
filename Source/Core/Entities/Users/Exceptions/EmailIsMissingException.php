<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

/**
 * Quan falta l'email.
 */
class EmailIsMissingException extends LocalizedException
{
    protected $code = 'ERR_EMAIL_ALREADY_IN_USE';

    protected $messages = [
        LanguageCode::English => 'The email is missing.',
        LanguageCode::Spanish => 'Falta el correo.',
        LanguageCode::Catalan => 'Falta el correu.',
    ];
}
