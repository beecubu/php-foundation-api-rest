<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions;

use Exception;

/**
 * Cuando el tipo de usuario devuelto no es del tipo que se esperaba.
 */
class UnexpectedUserInstanceException extends Exception
{
    public function __construct(string $expected, string $received)
    {
        parent::__construct("Unexpected user instance of user (Expected: '$expected' Received: $received)");
    }
}
