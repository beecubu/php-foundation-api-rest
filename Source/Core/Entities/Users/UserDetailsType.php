<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users;

use Beecubu\Foundation\Core\Enum;

class UserDetailsType extends Enum
{
    public const Admin = 'admin';
}
