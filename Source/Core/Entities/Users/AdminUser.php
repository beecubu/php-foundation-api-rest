<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users;

/**
 * Representa un usuario admin.
 */
class AdminUser extends ApiUser
{
    public function getType(): string
    {
        return UserDetailsType::Admin;
    }
}
