<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users;

use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\InvalidIdentifierException;
use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\Phone\PhoneNumber;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\UnexpectedUserInstanceException;
use Beecubu\Foundation\ApiRest\Core\Persistence\UserDB;
use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\MongoDB\ExplicitEntity;
use DateTime;
use Exception;
use MongoDB\BSON\ObjectId;
use stdClass;

/**
 * Defineix a un usuari de la plataforma.
 *
 * @property-read string $id L'id de l'usuari.
 * @property string $email L'email de l'usuari.
 * @property string $name El nom.
 * @property-read PhoneNumber $phone El número de telèfon.
 * @property string $language El codi de l'idioma d'aquest client.
 * @property-read DateTime $creationDate El dia i hora que es va crear.
 *
 * @method hasEmail()
 * @method hasPhone()
 * @method hasLanguage()
 * @method hasCountryCode()
 */
abstract class User extends ExplicitEntity
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'email'                => [Property::READ_WRITE, Property::IS_STRING],
            'name'                 => [Property::READ_WRITE, Property::IS_STRING],
            'phone'                => [Property::READ_WRITE, Property::IS_OBJECT, PhoneNumber::class],
            'language'             => [Property::READ_WRITE, Property::IS_ENUM, LanguageCode::class],
            'creationDate'         => [Property::READ_ONLY, Property::IS_DATE],
            'lastModificationDate' => [Property::READ_ONLY, Property::IS_DATE],
        ];
    }

    // JSON methods

    protected function excludedJsonProperties(): void
    {
        parent::excludedJsonProperties();
        // append some extra excluded fields
        $this->excJsonProp = array_merge($this->excJsonProp, [
            'id',
            'lastModificationDate',
        ]);
    }

    // Setters / getters

    /**
     * Obtiene la fecha de creación del usuario.
     *
     * @return DateTime
     *
     * @throws Exception
     */
    protected function getCreationDate(): DateTime
    {
        if ( ! isset($this->ivars['creationDate']))
        {
            $this->ivars['creationDate'] = new DateTime('@'.(new ObjectId($this->id))->getTimestamp());
        }
        // get the creation date
        return $this->ivars['creationDate'];
    }

    // Class constructor

    /**
     * @throws InvalidIdentifierException
     */
    protected static function instanceByRawDataId($id)
    {
        if ($id instanceof stdClass && isset($id->id))
        {
            return static::getDbClass()->byId($id->id);
        }
        elseif (is_array($id) && isset($id['id']))
        {
            return static::getDbClass()->byId($id['id']);
        }
        elseif (is_string($id) || $id instanceof ObjectId)
        {
            return static::getDbClass()->byId($id);
        }
        throw new InvalidIdentifierException();
    }

    public function __construct()
    {
        parent::__construct();
        // some initializer
        $this->set_ivar('creationDate', new DateTime());
    }

    // DB methods

    /**
     * Obté la classe per la connexió a la BD.
     *
     * @return UserDB
     */
    protected static function getDbClass()
    {
        return UserDB::current();
    }

    // Public methods

    /**
     * Devuelve todos los usuarios de la BD.
     *
     * @return User[]
     */
    public static function allUsers(): array
    {
        return static::getDbClass()->allUsers();
    }

    /**
     * Devuelve los usuarios filtrados de la DB.
     *
     * @param int $limit El número de resultados.
     * @param int $skip El numero de resultados a ignorar.
     * @param string|null $class El tipo de usuario que se busca.
     *
     * @return array
     */
    public static function filteredUsers(int $limit, int $skip, ?string $class): array
    {
        return static::getDbClass()->filteredUsers($limit, $skip, $class);
    }

    /**
     * Obté un usuari a partir del seu id.
     *
     * @param string $id Id del usuario.
     *
     * @return static
     *
     * @throws UnexpectedUserInstanceException
     */
    public static function userById(string $id): ?self
    {
        if ($user = static::getDbClass()->byId($id))
        {
            if ($user instanceof static) return $user;
            // ops... unexpected user instance
            throw new UnexpectedUserInstanceException(static::class, $user->getClassName());
        }
        // not found
        return null;
    }

    /**
     * Devuelve la información más básica del usuario.
     *
     * @return stdClass
     */
    public function basicInformation(): stdClass
    {
        $data = new stdClass();
        $data->name = $this->name;
        return $data;
    }

    /**
     * Guarda los cambios.
     */
    public function save(): void
    {
        static::getDbClass()->save($this);
    }
}
