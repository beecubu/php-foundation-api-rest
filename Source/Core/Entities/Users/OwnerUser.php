<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users;

use Beecubu\Foundation\Core\Property;

/**
 * Representa un usuario principal.
 *
 * @property-read string $id L'id.
 * @property-read string $publicId L'id públic de l'usuari.
 * @property-read bool $suspended TRUE = No pot accedir, FALSE = tot be.
 */
class OwnerUser extends User
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'publicId'  => [Property::READ_ONLY, Property::IS_STRING],
            'suspended' => [Property::READ_ONLY, Property::IS_BOOLEAN],
        ];
    }

    /**
     * Obté un usuari propietari a partir del seu públic id.
     *
     * @param string $publicId El públic id.
     *
     * @return static|null
     */
    public static function userByPublicId(string $publicId): ?self
    {
        return static::getDbClass()->byPublicId($publicId);
    }
}
