<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Users;

use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\EmailAlreadyInUseException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\EmailIsMissingException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\PasswordIsMissingException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\PasswordIsTooShortException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\UnexpectedUserInstanceException;
use Beecubu\Foundation\Core\Property;
use DateTime;
use stdClass;

/**
 * Define a un usuario de la plataforma que puede realizar login.
 *
 * @property-read string $ownerId L'id del seu propietari.
 * @property-write string $password La contrasenya.
 * @property-read string $flashId Id temporal per iniciar la sessió.
 * @property-read bool $suspended TRUE = L'usuari no pot accedir, FALSE = tot normal.
 * @property-read DateTime $lastLoginDate L'última vegada que ha iniciat sessió.
 *
 * @method bool isSuspended()
 *
 * @method bool hasPassword()
 */
abstract class ApiUser extends User
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'ownerId'       => [Property::READ_ONLY, Property::IS_STRING],
            'password'      => [Property::WRITE_ONLY, Property::IS_STRING],
            'flashId'       => [Property::READ_ONLY, Property::IS_STRING],
            'suspended'     => [Property::READ_ONLY, Property::IS_BOOLEAN],
            'lastLoginDate' => [Property::READ_ONLY, Property::IS_DATE],
        ];
    }

    // JSON methods

    /**
     * @param string $value El password.
     *
     * @throws PasswordIsTooShortException
     */
    protected function setPassword(string $value): void
    {
        if (strlen($value) < 6)
        {
            throw new PasswordIsTooShortException();
        }
        // set this value
        $this->set_ivar('password', password_hash($value, PASSWORD_BCRYPT));
    }

    protected function excludedJsonProperties(): void
    {
        parent::excludedJsonProperties();
        // append some extra excluded fields
        $this->excJsonProp = array_merge($this->excJsonProp, [
            'password',
            'flashId',
            'suspended',
            'lastLoginDate',
        ]);
    }

    // Factory methods

    /**
     * Tots els usuaris d'un propietari.
     *
     * @param OwnerUser $ownerUser El propietari.
     *
     * @return ApiUser[]
     */
    public static function allUsersByOwner(OwnerUser $ownerUser): array
    {
        if ($users = static::getDbClass()->allUsersByOwner($ownerUser))
        {
            return $users;
        }
        return [];
    }

    /**
     * Obtiene un usuario a partir de su email y contraseña.
     *
     * @param string $email El correo.
     * @param string $password La contraseña.
     *
     * @return ApiUser|null
     */
    public static function byEmailAndPassword(string $email, string $password): ?ApiUser
    {
        if ($user = static::getDbClass()->byEmail($email) and $user->passwordVerify($password))
        {
            return $user;
        }
        return null;
    }

    /**
     * Obtiene un usuario a partir de su flash Id.
     *
     * @param string $id El flash id del usuario.
     *
     * @return ApiUser|null
     */
    public static function byFlashId(string $id): ?ApiUser
    {
        if ($user = static::getDbClass()->byFlashId($id))
        {
            return $user;
        }
        return null;
    }

    // Public methods

    /**
     * Comprova si la contrasenya és correcte.
     *
     * @param string $password La contrasenya a comprovar.
     *
     * @return bool
     */
    public function passwordVerify(string $password): bool
    {
        return password_verify($password, $this->ivars['password']);
    }

    /**
     * Actualiza la fecha del último inicio de sesión.
     *
     * @param bool $autoSave TRUE = Guarda los cambios, FALSE = no los guarda.
     *
     * @throws EmailAlreadyInUseException
     * @throws EmailIsMissingException
     * @throws PasswordIsMissingException
     */
    public function updateLastLoginDate(bool $autoSave = true): void
    {
        $this->set_ivar('lastLoginDate', new DateTime());
        // should save?
        if ($autoSave)
        {
            $this->save();
        }
    }

    /** @inheritDoc
     *
     * @throws EmailAlreadyInUseException
     * @throws EmailIsMissingException
     * @throws PasswordIsMissingException
     */
    public function save(): void
    {
        // check email
        if ($this->hasEmail())
        {
            if ( ! static::getDbClass()->emailCanBeUsed($this->email, $this->id))
            {
                throw new EmailAlreadyInUseException();
            }
        }
        else // no email?
        {
            throw new EmailIsMissingException();
        }
        // check password
        if ( ! $this->hasPassword())
        {
            throw new PasswordIsMissingException();
        }
        // save user
        parent::save();
    }

    /**
     * Genera un nuevo flash Id.
     */
    public function generateFlashId(): void
    {
        $this->set_ivar('flashId', sha1(uniqid('', true)));
    }

    /**
     * Elimina el usuario.
     */
    public function delete(): void
    {
        static::getDbClass()->delete($this);
    }

    /**
     * Retorna l'usuari propietari.
     *
     * @return OwnerUser|null
     *
     * @throws UnexpectedUserInstanceException
     */
    public function getOwnerUser(): ?OwnerUser
    {
        if ($this->ownerId)
        {
            return OwnerUser::userById($this->ownerId);
        }
        return null;
    }

    /**
     * Devuelve la información más básica del usuario.
     *
     * @return stdClass
     */
    public function basicInformation(): stdClass
    {
        $data = parent::basicInformation();
        $data->type = $this->getType();
        return $data;
    }

    /**
     * Retorna el tipus d'usuari.
     *
     * @return string
     */
    public abstract function getType(): string;
}
