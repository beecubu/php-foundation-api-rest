<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Exceptions;

use Exception;

/**
 * S'ha fet servir un create que no es pot fer servir.
 */
class DisabledCreateMethodException extends Exception
{
}
