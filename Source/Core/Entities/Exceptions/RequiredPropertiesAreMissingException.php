<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class RequiredPropertiesAreMissingException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_REQUIRED_PROPERTIES_MISSING';

    /** @var array $errors */
    protected $errors;

    protected $messages = [
        LanguageCode::English => 'Those required properties are missing: %s',
        LanguageCode::Spanish => 'Falta asignar esas propiedades requeridas: %s',
        LanguageCode::Catalan => 'Falta assignar aquestes propietats requerides: %s',
    ];

    /**
     * @inheritDoc
     */
    public function __construct(array $errors)
    {
        parent::__construct();
        // full fill the message
        $this->errors = $errors;
    }

    /**
     * @inheritDoc
     */
    public function getLocalizedMessage(): string
    {
        return sprintf(parent::getLocalizedMessage(), implode("\n", $this->errors));
    }

    /**
     * Devuelve los errores detectados.
     *
     * @return string[]
     */
    public function getData(): array
    {
        return $this->errors;
    }
}
