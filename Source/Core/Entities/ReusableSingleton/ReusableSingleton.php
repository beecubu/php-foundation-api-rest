<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\ReusableSingleton;

abstract class ReusableSingleton
{
    protected static $instance = [];

    abstract protected static function instanceKey(): string;

    /**
     * Devuelve la instancia del controlador.
     *
     * @return static
     */
    public static function current()
    {
        // get the singleton key
        $key = static::instanceKey();
        // first time?
        if ( ! isset(self::$instance[$key]))
        {
            self::$instance[$key] = new static();
        }
        return self::$instance[$key];
    }
}
