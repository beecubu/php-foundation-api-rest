<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\IPInfo;

use Beecubu\Foundation\ApiRest\Core\Persistence\IPInfoCacheDB;
use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\MongoDB\PlainEntity;
use DateTime;
use stdClass;

defined('IPINFO_IPSTACK_API_KEYS') or define('IPINFO_IPSTACK_API_KEYS', []);

/**
 * @property-read string $ip La IP.
 * @property-read string $country El codi del país.
 * @property-read DateTime $lastUpdate L'última vegada que s'ha afegit a la BD.
 */
class IPInfo extends PlainEntity
{
    protected static $ivar_publicId = 'ip';

    /** @var IPInfo|null */
    private static $info = null;

    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'country'    => [Property::READ_ONLY, Property::IS_STRING],
            'lastUpdate' => [Property::READ_ONLY, Property::IS_DATE],
        ];
    }

    // Class factories

    /**
     * Obté la IP de l'usuari.
     *
     * @return string La IP de l'usuari.
     */
    public static function clientIP(): string
    {
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $HTTP_CLIENT_IP = $_SERVER['HTTP_CLIENT_IP'];
        elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $HTTP_CLIENT_IP = $_SERVER['HTTP_X_FORWARDED_FOR'];
        elseif (isset($_SERVER['HTTP_X_FORWARDED']))
            $HTTP_CLIENT_IP = $_SERVER['HTTP_X_FORWARDED'];
        elseif (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $HTTP_CLIENT_IP = $_SERVER['HTTP_FORWARDED_FOR'];
        elseif (isset($_SERVER['HTTP_FORWARDED']))
            $HTTP_CLIENT_IP = $_SERVER['HTTP_FORWARDED'];
        elseif (isset($_SERVER['REMOTE_ADDR']))
            $HTTP_CLIENT_IP = $_SERVER['REMOTE_ADDR'];
        else // no fu**ing idea…
            $HTTP_CLIENT_IP = 'UNKNOWN';
        return $HTTP_CLIENT_IP;
    }

    /**
     * Obté informació d'una IP.
     *
     * @param string|null $ip La IP de la que es vol la informació, NULL = Automàtic.
     *
     * @return static
     */
    public static function get(?string $ip = null): self
    {
        // automatic IP?
        if ( ! $ip) $ip = self::clientIP();
        // is the information cached for this IP?
        if (self::$info && self::$info->ip === $ip) return self::$info;
        // get new information
        if ($ip && in_array($ip, ['UNKNOWN', '::1', '127.0.0.1']) === false)
        {
            // info exists but is too old? then "drop it"...
            if (self::$info = IPInfoCacheDB::ipInfoByIP($ip) and ! self::$info->isVeryOld())
            {
                return self::$info;
            }
            // TRY 1) ip2c.org service
            $data = null;
            $ip2cData = @file_get_contents('https://ip2c.org/'.$ip);
            // parse data
            switch ($ip2cData[0])
            {
                case '0':
                    // Something wrong
                    break;
                case '1':
                    $data = new stdClass();
                    $data->country_code = explode(';', $ip2cData)[1];
                    break;
                case '2':
                    // Not found in database;
                    break;
            }
            // TRY 2) api.ipstack.com service
            if ( ! $data)
            {
                $ipApiTokens = IPINFO_IPSTACK_API_KEYS;
                // try each ipapi token
                foreach ($ipApiTokens as $token)
                {
                    $data = json_decode(@file_get_contents("http://api.ipstack.com/$ip?access_key=$token"));
                    if ($data && ($data->success ?? true)) break;
                    $data = null;
                }
                // TRY 3)
                if ( ! $data) $data = json_decode(@file_get_contents("https://ipapi.co/$ip/json/"));
            }
            // info to display
            if ($data)
            {
                self::$info = new self();
                self::$info->set_ivar('ip', $ip);
                self::$info->set_ivar('country', strtoupper($data->country_code));
                self::$info->set_ivar('lastUpdate', new DateTime());
                // save changes
                IPInfoCacheDB::saveIPInfo(self::$info);
            }
        }
        // no info? the return default info
        if ( ! self::$info)
        {
            self::$info = new self();
            self::$info->set_ivar('ip', '127.0.0.1');
            self::$info->set_ivar('country', 'ES');
        }
        // return info
        return self::$info;
    }

    // Public methods

    /**
     * Comprova si la informació és una mica vella.
     *
     * @return bool TRUE = És una mica vella, FALSE = no.
     */
    public function isVeryOld(): bool
    {
        return $this->lastUpdate && $this->lastUpdate > new DateTime('-48h');
    }
}
