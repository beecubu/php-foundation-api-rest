<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\Logger;

use Throwable;

/**
 * Gestiona els logs d'errors i altres. Aquesta classe no fa res, és un placeholder per implementar
 * en els projectes.
 */
class Logger
{
    protected static $instance;

    public static function current()
    {
        if ( ! static::$instance instanceof static)
        {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * Log de quan hi ha un error.
     *
     * @param Throwable $exception
     *
     * @return void
     */
    public function error(Throwable $exception): void
    {
        // Override this method in a subclass to implement custom error logging.
    }
}
