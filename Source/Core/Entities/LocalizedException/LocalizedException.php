<?php

namespace Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\Language;
use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Exception;

/**
 * Excepciones multi-idioma.
 */
class LocalizedException extends Exception
{
    /** @var array<string, string> $messages */
    protected $messages = [];

    /**
     * @inheritDoc
     */
    public function __construct()
    {
        parent::__construct($this->messages[LanguageCode::English] ?? '');
    }

    /**
     * Devuelve el mensaje de error en el idioma configurado de la sesión.
     *
     * @return string El mensaje de error.
     */
    public function getLocalizedMessage(): string
    {
        if ( ! $this->messages)
        {
            return $this->getMessage();
        }
        // get the localized message
        if ($locale = Language::currentLanguage() and isset($this->messages[$locale]))
        {
            return $this->messages[$locale];
        }
        // not found? then return the first one
        return $this->messages[array_keys($this->messages)[0]];
    }

    /**
     * Información adicional relacionada con la excepción y que se quiera informar.
     *
     * @return null
     */
    public function getData()
    {
        return null;
    }
}
