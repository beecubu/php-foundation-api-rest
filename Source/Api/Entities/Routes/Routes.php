<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Routes;

use Phroute\Phroute\RouteCollector;

/**
 * Representa un conjunt de rutes, normalment d'un controlador.
 */
abstract class Routes
{
    const ROUTE = null;

    abstract public static function routes(RouteCollector $router): void;
}
