<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Routes\REST;

use Beecubu\Foundation\ApiRest\Api\Controllers\CRUDController\CRUDController;
use Beecubu\Foundation\ApiRest\Api\Entities\Routes\Routes;
use Phroute\Phroute\RouteCollector;

/**
 * Rutas relacionadas con los menus.
 */
abstract class REST extends Routes
{
    /**
     * @return CRUDController
     */
    abstract protected static function getControllerClass(): CRUDController;

    public static function routes(RouteCollector $router): void
    {
        $router->get(static::ROUTE.'canManage/{action}?', function (?string $action = null)
        {
            return static::getControllerClass()->canManage($action);
        });

        $router->post(static::ROUTE, function ()
        {
            return static::getControllerClass()->create();
        });

        $router->get(static::ROUTE.'all', function ()
        {
            return static::getControllerClass()->all();
        });

        $router->get(static::ROUTE.'count', function ()
        {
            return static::getControllerClass()->count();
        });

        $router->get(static::ROUTE.'active/all', function ()
        {
            return static::getControllerClass()->allActive();
        });

        $router->get(static::ROUTE.'active/count', function ()
        {
            return static::getControllerClass()->countActive();
        });

        $router->post(static::ROUTE.'filtered', function ()
        {
            return static::getControllerClass()->filtered();
        });

        $router->get(static::ROUTE.'{id}', function (string $id)
        {
            return static::getControllerClass()->get($id);
        });

        $router->put(static::ROUTE.'{id}', function (string $id)
        {
            return static::getControllerClass()->edit($id);
        });

        $router->delete(static::ROUTE.'{id}', function (string $id)
        {
            return static::getControllerClass()->delete($id);
        });
    }
}
