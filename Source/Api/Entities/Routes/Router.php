<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Routes;

use Phroute\Phroute\RouteCollector;

/**
 * Representa la classe amb les rutes de l'API.
 */
abstract class Router
{
    protected static $instance = null;

    /**
     * Init the "default" instance.
     *
     * @return self
     */
    public static function instance(): self
    {
        if (!self::$instance) self::$instance = new static();
        return self::$instance;
    }

    /**
     * Configure the main v1 API routes.
     *
     * GET    -> Get
     * POST   -> Add
     * PUT    -> Edit
     * DELETE -> Delete
     *
     * @param RouteCollector $router The route collector.
     */
    abstract public function routes(RouteCollector $router): void;
}
