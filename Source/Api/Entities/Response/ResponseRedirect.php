<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Response;

use Beecubu\Foundation\Core\Property;

/**
 * Representa una resposta REDIRECT de la Api.
 *
 * @property boolean $htmlRedirect TRUE = Inclou un HTML amb un redirect, FALSE = no retorna res.
 *
 * @method bool isHtmlRedirect()
 */
class ResponseRedirect extends Response
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'htmlRedirect' => [Property::READ_WRITE, Property::IS_BOOLEAN],
        ];
    }

    /**
     * Realiza el redirect.
     */
    public function send(): void
    {
        if ($this->isHtmlRedirect())
        {
            echo '<!DOCTYPE html><html><head>';
            echo '<meta http-equiv="refresh" content="0; url=\''.$this->data.' \'" />';
            echo '</head><body></body></html>';
        }
        else
        {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: '.$this->data);
        }
    }
}
