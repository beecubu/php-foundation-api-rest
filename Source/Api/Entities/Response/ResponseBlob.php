<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Response;

/**
 * Representa una resposta BLOB de l'Api.
 */
class ResponseBlob extends Response
{
    /**
     * Envia la resposta.
     */
    public function send(): void
    {
        // set up the custom headers
        $this->customHeaders();
        // print the json
        echo $this->data;
    }
}
