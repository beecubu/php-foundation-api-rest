<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Response;

/**
 * Representa una resposta "simple" (nomes headers, sense contingut)
 */
class ResponsePlain extends Response
{
    /**
     * Realiza el redirect.
     */
    public function send(): void
    {
        // nothing to output
    }
}
