<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Response;

use Beecubu\Foundation\ApiRest\Api\Entities\Request\Request;
use Beecubu\Foundation\ApiRest\Core\Entities\Environment\Environment;
use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;
use Beecubu\Foundation\ApiRest\Core\Entities\Logger\Logger;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Session;
use Beecubu\Foundation\Core\Exceptions\SerializeIsNotSubclassOfSerializableException;
use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\Core\Serializable;
use stdClass;
use Throwable;

/**
 * Representa una resposta de l'Api.
 *
 * @property mixed $data El contingut demanat a l'API.
 * @property bool $success TRUE = S'ha produït un error, FALSE = tot ok.
 * @property int|string $code  El codi d'error.
 * @property string $message El missatge de l'error.
 * @property mixed $debug Informació adicional sobre l'error o altres informacions relacionades, només en modo DEV o Sandbox.
 * @property string[] $headers Listado de headers a enviar en la petición.
 *
 * @method bool hasHeaders()
 * @method bool hasData()
 */
class Response extends Serializable
{
    public $headers = [];

    /** @var Request */
    public $fromRequest = null;

    public $noContent = false;

    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'data'    => [Property::READ_WRITE, Property::IS_MIXED],
            'success' => [Property::READ_WRITE, Property::IS_BOOLEAN],
            'code'    => [Property::READ_WRITE, Property::IS_MIXED],
            'message' => [Property::READ_WRITE, Property::IS_STRING],
            'debug'   => [Property::READ_WRITE, Property::IS_MIXED],
        ];
    }

    /**
     * Crea una resposta de tot ok.
     *
     * @return static
     */
    public static function successResponse(): Response
    {
        $response = new static();
        // configure the response
        $response->success = true;
        // the error
        return $response;
    }

    /**
     * Crea una resposta de tot ok per sense contingut.
     *
     * @return static
     */
    public static function successNoContentResponse(): Response
    {
        $response = new static();
        // configure the response
        $response->success = true;
        $response->noContent = true;
        // the error
        return $response;
    }

    /**
     * Crea una resposta fallida (sense informació).
     *
     * @return static
     */
    public static function failedResponse(): Response
    {
        $response = new static();
        // configure the response
        $response->success = false;
        // the error
        return $response;
    }

    /**
     * Crea una resposta amb un error.
     *
     * @param Throwable $exception
     *
     * @return static
     */
    public static function responseWithThrowable(Throwable $exception): Response
    {
        $response = new static();
        // configure the response
        $response->success = false;
        $response->code = $exception->getCode();
        // is a localized exception?
        if ($exception instanceof LocalizedException)
        {
            $response->message = $exception->getLocalizedMessage();
            // has additional data to send?
            if ($data = $exception->getData())
            {
                $response->data = $data;
            }
        }
        else // is a standard exception (this is not a public exception)
        {
            $messages = [
                LanguageCode::English => 'An unexpected error occurred. Our team has been informed about this problem.',
                LanguageCode::Spanish => 'Se ha producido un error inesperado. Nuestro equipo ya ha sido informado acerca del problema.',
                LanguageCode::Catalan => 'S\'ha produït un error inesperat. El nostre equip ja ha sigut informat del problema.',
            ];
            // assign the message
            $response->message = $messages[Session::current()->locale ?? LanguageCode::English];
            // append debug information when is NOT LIVE
            if ( ! Environment::current()->isLiveMode())
            {
                $response->debug = new stdClass();
                $response->debug->exception = $exception->getMessage();
                $response->debug->trace = $exception->getTrace();
            }
        }
        // log this error
        Logger::current()->error($exception);
        // the error
        return $response;
    }

    protected function customHeaders(): void
    {
        foreach ($this->headers as $value)
        {
            header($value);
        }
    }

    /**
     * Envia la resposta.
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     */
    public function send(): void
    {
        // default headers
        header('Response-Type: api-response');
        header('Content-Type: application/json');
        header('Cache-Control: no-cache');
        // set up the custom headers
        $this->customHeaders();
        // is marked as 204?
        if ($this->noContent)
        {
            http_response_code(204);
            return;
        }
        // has filtered fields options specified?
        $response = $this;
        // has a "fields" option specified?
        if ($this->fromRequest && $this->hasData() && $fields = $this->fromRequest->getOption('fields'))
        {
            $excluding = $this->fromRequest->getOption('excluding') ?? false;
            // convert to JSON the response (except data field)
            $response = $this->json(['data']);
            // convert to JSON the response data (applying the fields filter)
            $response->data = $this->json(['data' => $fields], $excluding)->data ?? null;
        }
        // return the response as string
        echo json_encode($response);
    }
}
