<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Dispatcher;

use Beecubu\Foundation\ApiRest\Api\Entities\Request\Request;
use Beecubu\Foundation\ApiRest\Api\Entities\Response\Response;
use Beecubu\Foundation\ApiRest\Api\Entities\Routes\Router;
use Beecubu\Foundation\ApiRest\Api\Exceptions\InvalidResponseException;
use Beecubu\Foundation\ApiRest\Api\Exceptions\MethodNotAllowedException;
use Beecubu\Foundation\ApiRest\Api\Exceptions\MethodNotFoundException;
use Beecubu\Foundation\ApiRest\Core\Entities\Environment\Environment;
use Phroute\Phroute\Exception\HttpMethodNotAllowedException;
use Phroute\Phroute\Exception\HttpRouteNotFoundException;
use Phroute\Phroute\RouteCollector;
use Throwable;

defined('DISPATCHER_SEND_HEADERS') or define('DISPATCHER_SEND_HEADERS', true);
defined('DISPATCHER_ALLOWED_ORIGINS') or define('DISPATCHER_ALLOWED_ORIGINS', []);

class Dispatcher
{
    /** @var \Phroute\Phroute\Dispatcher $dispatcher */
    protected $dispatcher;

    /**
     * El dispatcher actual.
     *
     * @return static
     */
    public static function current(): Dispatcher
    {
        static $instance = null;
        if ( ! $instance)
        {
            $instance = new static();
        }
        return $instance;
    }

    /**
     * Class constructor.
     */
    public function __construct()
    {
        // configure routes
        $router = new RouteCollector();

        // configure the Api routes
        Router::instance()->routes($router);

        // send headers
        if (DISPATCHER_SEND_HEADERS)
        {
            // list of origins
            $http_origin = Environment::current()->getOrigin();
            $allowedOrigins = DISPATCHER_ALLOWED_ORIGINS;
            // validate if is an authorized origin
            foreach ($allowedOrigins as $preg)
            {
                if (preg_match_all($preg, $http_origin, $matches, PREG_SET_ORDER) > 0)
                {
                    header("Access-Control-Allow-Origin: $http_origin");
                }
            }
            // dispatcher
            header("Access-Control-Allow-Credentials: true");
            header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
            header("Access-Control-Expose-Headers: Response-Type");
            header("Allow: GET, POST, OPTIONS, PUT, DELETE");
        }

        // get the http method (GET is the default one)
        $method = $_SERVER['REQUEST_METHOD'] ?? 'GET';

        // if is an options request, then finish here
        if ($method == 'OPTIONS') die ();

        // create the dispatcher
        $this->dispatcher = new \Phroute\Phroute\Dispatcher($router->getData());
    }

    /**
     * Executa la ruta.
     *
     * @param string $method El mètode a executar.
     * @param string $uri La url a executar.
     * @param Request|null $request El request a executar.
     *
     * @return Response
     */
    public function dispatch(string $method, string $uri, ?Request $request = null): Response
    {
        try
        {
            // pre-fill some "fixed" information
            if ($request) Request::current()->fromRawData($request->rawData());
            // execute url
            $response = $this->dispatcher->dispatch($method, $uri);
            // is not a Response?
            if ( ! $response instanceof Response)
            {
                throw new InvalidResponseException();
            }
            // store the request into the response
            if ( ! $response->fromRequest)
            {
                $response->fromRequest = Request::current();
            }
        }
        catch (HttpRouteNotFoundException $e)
        {
            $response = Response::responseWithThrowable(new MethodNotFoundException());
        }
        catch (HttpMethodNotAllowedException $e)
        {
            $response = Response::responseWithThrowable(new MethodNotAllowedException());
        }
        catch (Throwable $e)
        {
            $response = Response::responseWithThrowable($e);
        }
        finally
        {
            return $response ?? Response::failedResponse();
        }
    }
}
