<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Request;

use Beecubu\Foundation\ApiRest\Api\Exceptions\InvalidRequestException;
use Beecubu\Foundation\Core\Exceptions\SerializeIsNotSubclassOfSerializableException;
use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\Core\Serializable;
use stdClass;

/**
 * Representa un request del servidor.
 *
 * @property mixed $data La informació que s'ha enviat al servidor.
 * @property mixed $options Opcions addicionals (i opcionals) d'aquesta petició.
 * @property array $files Fitxers adjunts (i opcionals) d'aquesta petició.
 */
class Request extends Serializable
{
    /** @var $this |null */
    protected static $instance = null;

    /**
     * Retorna la instància actual.
     */
    public static function current(): self
    {
        // first time?
        if ( ! self::$instance)
        {
            self::$instance = new static();
            // get the json send to server
            if ($json = json_decode(file_get_contents('php://input')))
            {
                self::$instance->fromJson($json);
            }
            elseif (isset($_POST))
            {
                self::$instance->fromJson((object)[
                    'data'    => isset($_POST['data']) ? json_decode($_POST['data']) : null,
                    'options' => isset($_POST['options']) ? json_decode($_POST['options']) : null,
                ]);
            }
            // assign files
            if (self::$instance && $_FILES)
            {
                self::$instance->files = $_FILES;
            }
        }
        // return the instance
        return self::$instance;
    }

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'data'    => [Property::READ_WRITE, Property::IS_MIXED],
            'options' => [Property::READ_WRITE, Property::IS_MIXED],
            'files'   => [Property::READ_WRITE, Property::IS_ARRAY],
        ];
    }

    /**
     * Obté el data del request.
     *
     * @return mixed El data del request.
     *
     * @throws InvalidRequestException
     */
    protected function getData()
    {
        if (isset($this->ivars['data']))
        {
            return $this->ivars['data'];
        }
        throw new InvalidRequestException();
    }

    /**
     * Retorna la instància actual fent servir el "data" com un objecte concret del tipus Serializable.
     * Nota: És un alias del "getDataAs".
     *
     * @param string $class
     *
     * @return mixed|null
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     */
    public static function as(string $class)
    {
        return static::current()->getDataAs($class);
    }

    /**
     * Obté el data de la sol·licitud sigui el que sigui, és a dir, si no hi ha "data" no passa res i pot ser qualsevol valor.
     *
     * @return mixed
     */
    public function optionalData()
    {
        return $this->ivars['data'] ?? null;
    }

    /**
     * Obté el data com un array.
     *
     * @return array|null
     */
    public function getDataAsArray(): array
    {
        if (is_array($this->data)) return $this->data;
        if ($this->data instanceof stdClass) return (array)$this->data;
        return [];
    }

    /**
     * Simplifica l'ús de les opcions, i obté el valor d'una opció concreta.
     *
     * @param string $key L'opció.
     *
     * @return mixed
     */
    public function getOption(string $key)
    {
        if ($this->options instanceof stdClass)
        {
            return $this->options->$key ?? null;
        }
        elseif (is_array($this->options))
        {
            return $this->options[$key] ?? null;
        }
        return null;
    }

    /**
     * Obté un valor del "data".
     *
     * @param string $key La clau del valor.
     *
     * @return mixed
     */
    public function getDataValue(string $key)
    {
        if ($this->data instanceof stdClass)
        {
            return $this->data->$key ?? null;
        }
        elseif (is_array($this->data))
        {
            return $this->data[$key] ?? null;
        }
        return null;
    }

    /**
     * Obté un valor del "data" com un objecte concret del tipus Serializable.
     *
     * @param string $class La classe resultant (Serializable).
     *
     * @return mixed|null
     *
     * @throws SerializeIsNotSubclassOfSerializableException
     */
    public function getDataAs(string $class)
    {
        if (is_subclass_of($class, Serializable::class))
        {
            return $this->data ? $class::instanceWithJson($this->data) : null;
        }
        // imposible serialize
        throw new SerializeIsNotSubclassOfSerializableException($class);
    }

    /**
     * Obté un fitxer de la petició.
     *
     * @param string $key La clau del fitxer.
 *
     * @return array|null
     */
    public function getFile(string $key): ?array
    {
        if (is_array($this->files))
        {
            return $this->files[$key] ?? null;
        }
        return null;
    }
}
