<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Filter;

use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\Core\Serializable;

/**
 * Representa l'ordre d'un camp.
 *
 * @property string $field El camp a ordenar.
 * @property string $sortDir La direcció a seguir.
 */
class FilterSortField extends Serializable
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'field'   => [Property::READ_WRITE, Property::IS_STRING],
            'sortDir' => [Property::READ_WRITE, Property::IS_ENUM, FilterSortDirection::class],
        ];
    }

    protected function getSortDir(): string
    {
        return $this->ivars['sortDir'] ?? FilterSortDirection::Ascending;
    }
}
