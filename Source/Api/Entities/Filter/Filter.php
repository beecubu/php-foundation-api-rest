<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Filter;

use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\Core\Serializable;

/**
 * Representa un filtre bàsic.
 *
 * @property int $pageIndex L'índex de la paginació.
 * @property int $pageSize El nombre de resultats per pàgina.
 * @property int $active TRUE = Només actius, FALSE = Només inactius, NULL = tots.
 * @property FilterSortField[] $sort Llistat amb el fields a ordenar i com ordenar-los.
 *
 * @method bool hasPageSize()
 * @method bool hasSort()
 *
 * @method int sortCount()
 */
class Filter extends Serializable
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'pageIndex' => [Property::READ_WRITE, Property::IS_INTEGER],
            'pageSize'  => [Property::READ_WRITE, Property::IS_INTEGER],
            'active'    => [Property::READ_WRITE, Property::IS_BOOLEAN],
            'sort'      => [Property::READ_WRITE, Property::IS_ARRAY, $this->getFilterSortFieldClass()],
        ];
    }

    protected function getFilterSortFieldClass(): string
    {
        return FilterSortField::class;
    }

    protected function getPageIndex(): int
    {
        return $this->ivars['pageIndex'] ?? 1;
    }
}
