<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Filter;

use Beecubu\Foundation\Core\Property;
use Beecubu\Foundation\Core\Serializable;

/**
 * Representa una resposta d'aplicar un filtre.
 *
 * @property int $count El nombre TOTAL de resultats després d'aplicar el filtre.
 * @property Serializable[] $results Els resultats després d'aplicar el filtre.
 */
class FilterResult extends Serializable
{
    // Properties definition

    protected function properties(): void
    {
        parent::properties();
        // append new properties
        $this->properties += [
            'count'   => [Property::READ_WRITE, Property::IS_INTEGER],
            'results' => [Property::READ_ONLY, Property::IS_ARRAY, Serializable::class],
        ];
    }

    /**
     * Afegeix un nou resultat.
     *
     * @param Serializable $result El resultat.
     *
     * @return void
     */
    public function addResult(Serializable $result): void
    {
        $this->push_ivar('results', $result);
    }
}
