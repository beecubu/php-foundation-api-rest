<?php

namespace Beecubu\Foundation\ApiRest\Api\Entities\Filter;

use Beecubu\Foundation\Core\Enum;

/**
 * Representa els tipus de direcció a l'hora d'ordenar.
 */
class FilterSortDirection extends Enum
{
    public const Ascending  = 'asc';
    public const Descending = 'des';

    /**
     * Converteix a integer la direcció.
     *
     * @param string $value La direcció a convertir.
     *
     * @return int
     */
    public static function toInt(string $value): int
    {
        if (static::validateValue($value))
        {
            return $value === static::Ascending ? -1 : 1;
        }
        return 0;
    }
}
