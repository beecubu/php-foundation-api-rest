<?php

namespace Beecubu\Foundation\ApiRest\Api\Routes;

use Beecubu\Foundation\ApiRest\Api\Controllers\SessionController;
use Beecubu\Foundation\ApiRest\Api\Entities\Routes\Routes;
use Phroute\Phroute\RouteCollector;

class Session extends Routes
{
    const ROUTE = 'api/v1/session/';

    public static function routes(RouteCollector $router): void
    {
        $router->post(static::ROUTE.'login', function ()
        {
            return static::getSessionController()->login();
        });

        $router->get(static::ROUTE.'logout', function ()
        {
            return static::getSessionController()->logout();
        });

        $router->get(static::ROUTE.'hello', function ()
        {
            return static::getSessionController()->hello();
        });

        $router->get(static::ROUTE.'me/basic', function ()
        {
            return static::getSessionController()->meBasic();
        });

        $router->get(static::ROUTE.'me', function ()
        {
            return static::getSessionController()->me();
        });

        $router->put(static::ROUTE.'me', function ()
        {
            return static::getSessionController()->saveMe();
        });
    }

    protected static function getSessionController(): SessionController
    {
        return SessionController::current();
    }
}
