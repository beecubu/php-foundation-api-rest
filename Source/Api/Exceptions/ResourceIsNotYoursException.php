<?php

namespace Beecubu\Foundation\ApiRest\Api\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class ResourceIsNotYoursException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_RESOURCE_IS_NOT_YOURS';

    protected $messages = [
        LanguageCode::English => 'The requested resource is not your.',
        LanguageCode::Spanish => 'El recurso solicitado no es tuyo.',
        LanguageCode::Catalan => 'El recurs sol·licitat no és teu.',
    ];
}
