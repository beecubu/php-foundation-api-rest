<?php

namespace Beecubu\Foundation\ApiRest\Api\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class InvalidResponseException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_INVALID_RESPONSE';

    protected $messages = [
        LanguageCode::English => 'Invalid response.',
        LanguageCode::Spanish => 'Respuesta incompleta o incorrecta de la API.',
        LanguageCode::Catalan => 'Resposta incompleta o incorrecte de la API.',
    ];
}
