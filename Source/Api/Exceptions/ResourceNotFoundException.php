<?php

namespace Beecubu\Foundation\ApiRest\Api\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class ResourceNotFoundException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_RESOURCE_NOT_FOUND';

    protected $messages = [
        LanguageCode::English => 'The requested resource do not exists.',
        LanguageCode::Spanish => 'El recurso solicitado no existe.',
        LanguageCode::Catalan => 'El recurs sol·licitat no existeix.',
    ];
}
