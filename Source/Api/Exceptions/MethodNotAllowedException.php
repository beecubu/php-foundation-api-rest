<?php

namespace Beecubu\Foundation\ApiRest\Api\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class MethodNotAllowedException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_REQUESTED_METHOD_NOT_ALLOWED';

    protected $messages = [
        LanguageCode::English => 'The requested API method is not allowed.',
        LanguageCode::Spanish => 'El método de la API solicitado no está permitido.',
        LanguageCode::Catalan => 'El mètode de la API sol·licitat no està permès.',
    ];
}
