<?php

namespace Beecubu\Foundation\ApiRest\Api\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class RequestNotPermittedException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_REQUEST_NOT_PERMITTED';

    protected $messages = [
        LanguageCode::English => 'The request is not permitted.',
        LanguageCode::Spanish => 'La petición no está permitido.',
        LanguageCode::Catalan => 'La petició no està permesa.',
    ];
}
