<?php

namespace Beecubu\Foundation\ApiRest\Api\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class MethodNotFoundException extends LocalizedException
{
    /** @var string $code */
    protected $code = 'ERR_REQUESTED_METHOD_NOT_FOUND';

    protected $messages = [
        LanguageCode::English => 'The requested API method do not exists.',
        LanguageCode::Spanish => 'El método de la API solicitado no existe.',
        LanguageCode::Catalan => 'El mètode de la API sol·licitat no existeix.',
    ];
}
