<?php

namespace Beecubu\Foundation\ApiRest\Api\Exceptions;

use Beecubu\Foundation\ApiRest\Core\Entities\Languages\LanguageCode;
use Beecubu\Foundation\ApiRest\Core\Entities\LocalizedException\LocalizedException;

class InvalidRequestException extends LocalizedException
{
    /** @var string $code  */
    protected $code = 'ERR_INVALID_REQUEST';

    protected $messages = [
        LanguageCode::English => 'Invalid or incomplete API request.',
        LanguageCode::Spanish => 'Petición incompleta o incorrecta a la API.',
        LanguageCode::Catalan => 'Petició incompleta o incorrecte a la API.',
    ];
}
