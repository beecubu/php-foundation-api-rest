<?php

namespace Beecubu\Foundation\ApiRest\Api\Controllers\CRUDController;

use Beecubu\Foundation\ApiRest\Api\Entities\Request\Request;
use Beecubu\Foundation\ApiRest\Api\Entities\Response\Response;
use Beecubu\Foundation\ApiRest\Api\Exceptions\ResourceIsNotYoursException;
use Beecubu\Foundation\ApiRest\Api\Exceptions\ResourceNotFoundException;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\InvalidIdentifierException;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\TryingToDeleteResourceInUseException;
use Beecubu\Foundation\ApiRest\Core\Entities\Entity\Exceptions\TryingToDeleteResourceInUseWithoutConfirmationException;
use Beecubu\Foundation\ApiRest\Core\Entities\Exceptions\RequiredPropertiesAreMissingException;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions\SessionForbiddenAccessException;
use Beecubu\Foundation\ApiRest\Core\Entities\ReusableSingleton\ReusableSingleton;
use Beecubu\Foundation\ApiRest\Core\Controllers\CRUDController\CRUDController as Controller;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\UnexpectedUserInstanceException;
use stdClass;

/**
 * Controlador CRUD estàndard.
 */
abstract class CRUDController extends ReusableSingleton
{
    /**
     * L'objecte CRUDController a usar.
     *
     * @return Controller
     */
    abstract protected function getClass(): Controller;

    /**
     * Retorna si l'usuari actual pot gestionar el controlador (Create/Edit/Delete).
     *
     * @param string|null $action L'action de referencia a comprovar.
     *
     * @return Response
     */
    public function canManage(?string $action): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getClass()->canManage($action);
        return $response;
    }

    /**
     * Crea un objecte nou.
     *
     * @return Response
     *
     * @throws RequiredPropertiesAreMissingException
     * @throws SessionForbiddenAccessException
     * @throws UnexpectedUserInstanceException
     */
    public function create(): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getClass()->create(Request::current()->data);
        return $response;
    }

    /**
     * Obté un objecte existent.
     *
     * @param string $id L'id de l'objecte.
     *
     * @return Response
     *
     * @throws InvalidIdentifierException
     * @throws ResourceIsNotYoursException
     * @throws ResourceNotFoundException
     * @throws UnexpectedUserInstanceException
     */
    public function get(string $id): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getClass()->get($id);
        return $response;
    }

    /**
     * Obté tots els objectes disponibles.
     *
     * @return Response
     *
     * @throws UnexpectedUserInstanceException
     */
    public function all(): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getClass()->all();
        return $response;
    }

    /**
     * Oté el recompte d'objectes disponibles.
     *
     * @return Response
     *
     * @throws UnexpectedUserInstanceException
     */
    public function count(): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getClass()->count();
        return $response;
    }

    /**
     * Obté tots els objectes ACTIUS disponibles.
     *
     * @return Response
     *
     * @throws UnexpectedUserInstanceException
     */
    public function allActive(): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getClass()->allActive();
        return $response;
    }

    /**
     * Oté el recompte d'objectes ACTIUS disponibles.
     *
     * @return Response
     *
     * @throws UnexpectedUserInstanceException
     */
    public function countActive(): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getClass()->countActive();
        return $response;
    }

    /**
     * Obté els objectes aplicant un filtre.
     *
     * @return Response
     *
     * @throws UnexpectedUserInstanceException
     */
    public function filtered(): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getClass()->filtered(Request::current()->data ?? new stdClass());
        return $response;
    }

    /**
     * Modifica un objecte existent.
     *
     * @param string $id L'id de l'objecte a modificar.
     *
     * @return Response
     *
     * @throws InvalidIdentifierException
     * @throws RequiredPropertiesAreMissingException
     * @throws ResourceIsNotYoursException
     * @throws ResourceNotFoundException
     * @throws SessionForbiddenAccessException
     * @throws UnexpectedUserInstanceException
     */
    public function edit(string $id): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getClass()->edit($id, Request::current()->data);
        return $response;
    }

    /**
     * Elimina un objecte existent.
     *
     * @param string $id L'id de l'objecte a eliminar.
     *
     * @return Response
     *
     * @throws InvalidIdentifierException
     * @throws ResourceIsNotYoursException
     * @throws ResourceNotFoundException
     * @throws SessionForbiddenAccessException
     * @throws TryingToDeleteResourceInUseException
     * @throws TryingToDeleteResourceInUseWithoutConfirmationException
     * @throws UnexpectedUserInstanceException
     */
    public function delete(string $id): Response
    {
        $response = Response::successResponse();
        $response->success = $this->getClass()->delete($id, Request::current()->getOption('confirm') ?? false);
        return $response;
    }
}
