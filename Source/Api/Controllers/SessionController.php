<?php

namespace Beecubu\Foundation\ApiRest\Api\Controllers;

use Beecubu\Foundation\ApiRest\Api\Entities\Request\Request;
use Beecubu\Foundation\ApiRest\Api\Entities\Response\Response;
use Beecubu\Foundation\ApiRest\Core\Controllers\SessionController as Controller;
use Beecubu\Foundation\ApiRest\Core\Entities\ReusableSingleton\ReusableSingleton;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions\InvalidSessionAuthException;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions\MissingSessionAuthException;
use Beecubu\Foundation\ApiRest\Core\Entities\Session\Exceptions\SessionForbiddenAccessException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\EmailAlreadyInUseException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\EmailIsMissingException;
use Beecubu\Foundation\ApiRest\Core\Entities\Users\Exceptions\PasswordIsMissingException;
use Exception;
use stdClass;

/**
 * Gestió bàsica de la sessió d'un usuari.
 */
class SessionController extends ReusableSingleton
{
    protected static function instanceKey(): string
    {
        return self::class;
    }

    /**
     * Obté el controlador de la sessió.
     *
     * @return Controller
     */
    protected function getController(): Controller
    {
        return Controller::current();
    }

    /**
     * Obté la informació a retornar després d'iniciar sessió.
     *
     * @return stdClass
     */
    protected function getLoginDataResponse(): stdClass
    {
        return $this->getController()->getUser()->basicInformation();
    }

    /**
     * Inicia sessió.
     *
     * @return Response
     * @throws InvalidSessionAuthException
     * @throws SessionForbiddenAccessException
     * @throws Exception
     */
    public function login(): Response
    {
        $request = Request::current();
        // try to log-in
        $this->getController()->login($request->getDataValue('email') ?? '', $request->getDataValue('password') ?? '');
        // all ok
        $response = Response::successResponse();
        $response->data = $this->getLoginDataResponse();
        return $response;
    }

    /**
     * Tanca la sessió.
     *
     * @return Response
     *
     * @throws Exception
     */
    public function logout(): Response
    {
        $this->getController()->logout();
        // all ok
        return Response::successResponse();
    }

    /**
     * Comprova si hi ha una sessió activa.
     *
     * @return Response
     *
     * @throws MissingSessionAuthException
     */
    public function hello(): Response
    {
        $this->getController()->hello();
        // all ok
        return Response::successResponse();
    }

    /**
     * Obté la informació bàsica de l'usuari.
     *
     * @return Response
     *
     * @throws MissingSessionAuthException
     */
    public function meBasic(): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getController()->meBasic();
        return $response;
    }

    /**
     * Obté el perfil de l'usuari actual.
     *
     * @return Response
     *
     * @throws MissingSessionAuthException
     */
    public function me(): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getController()->me();
        return $response;
    }

    /**
     * Guarda els canvis del perfil.
     *
     * @return Response
     *
     * @throws MissingSessionAuthException
     * @throws EmailAlreadyInUseException
     * @throws EmailIsMissingException
     * @throws PasswordIsMissingException
     */
    public function saveMe(): Response
    {
        $response = Response::successResponse();
        $response->data = $this->getController()->saveProfile(Request::current()->data);
        return $response;
    }
}
